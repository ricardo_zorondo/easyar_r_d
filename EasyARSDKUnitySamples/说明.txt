本包中包含EasyAR的Unity样例。
请阅读《使用必读》来了解应该如何来使用这些样例。

样例:
  HelloAR
    - 演示如何创建第一个EasyAR应用
    - 演示使用EasyAR在target上面显示3D内容和视频的最简单的方法

  HelloARTarget
    - 演示创建target的不同方法
    - 演示如何动态创建target

  HelloARVideo
    - 演示如何使用EasyAR加载并在target上播放视频
    - 演示本地视频播放
    - 演示透明视频播放
    - 演示流媒体视频播放

  TargetOnTheFly
    - 演示如何直接从相机图像中实时创建target并load到tracker中

  Coloring3D
    - 演示如何创建“AR涂涂乐”，使绘图书中的图像实时“转换”成3D

图片Markers:
  你可以在这些文件夹和其子文件夹中找到样例中使用的图片marker
    - HelloAR/Assets/StreamingAssets
    - HelloARTarget/Assets/StreamingAssets
    - HelloARVideo/Assets/StreamingAssets
    - Coloring3D/Assets/StreamingAssets

  所有这些marker（除了Coloring3D中的）都可以使用“视+”通过云识别来玩。这里面许多是有趣的AR游戏。
  下载 视+ ，一起来玩吧！
    http://www.sightp.com/
    http://www.sightp.com/view/downloadapp.html
