This package contains Unity samples for EasyAR.
Please read "user manual" for how to use.

Samples:
  HelloAR
    - Demonstrate how to create the first EasyAR app
    - Demonstrate the simplest way to display 3D contents and video on top of target using EasyAR

  HelloARTarget
    - Demonstrate different methods to create targets
    - Demonstrate how to dynamically create targets

  HelloARVideo
    - Demonstrate how to load and play video on top of target using EasyAR
    - Demonstrate local video playback
    - Demonstrate transparent video playback
    - Demonstrate streaming video playback

  TargetOnTheFly
    - Demonstrate how to create image target directly from real-time camera image and load it into tracker as a target

  Coloring3D
    - Demonstrate how to create a coloring book and "convert" book image into 3D at real-time

Image Markers:
  You can find image markers under these folders and their subfolders
    - HelloAR/Assets/StreamingAssets
    - HelloARTarget/Assets/StreamingAssets
    - HelloARVideo/Assets/StreamingAssets
    - Coloring3D/Assets/StreamingAssets

  All marker (except the on in Coloring3D) can be played with SightPlus using cloud recognition. Many of them are interesting AR Games.
  Download SightPlus and enjoy!
    http://www.sightp.com/
    http://www.sightp.com/view/downloadapp.html
