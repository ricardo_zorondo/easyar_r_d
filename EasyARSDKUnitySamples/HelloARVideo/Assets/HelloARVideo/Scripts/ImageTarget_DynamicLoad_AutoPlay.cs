﻿/**
* Copyright (c) 2015 VisionStar Information Technology (Shanghai) Co., Ltd. All Rights Reserved.
* EasyAR is the registered trademark or trademark of VisionStar Information Technology (Shanghai) Co., Ltd in China
* and other countries for the augmented reality technology developed by VisionStar Information Technology (Shanghai) Co., Ltd.
*/

using UnityEngine;
using EasyAR;

namespace EasyARSample
{
    public class ImageTarget_DynamicLoad_AutoPlay : ImageTargetBehaviour, ITargetEventHandler
    {
        private string video = @"http://7xl1ve.media1.z0.glb.clouddn.com/sdkvideo/EasyARSDKShow201520.mp4";

        protected override void Start()
        {
            base.Start();
            LoadVideo();
            HideObjects(transform);
        }

        public void LoadVideo()
        {
            GameObject subGameObject = Instantiate(Resources.Load("VideoPlayer", typeof(GameObject))) as GameObject;
            subGameObject.transform.parent = this.transform;
            subGameObject.transform.localPosition = new Vector3(0, 0.225f, 0);
            subGameObject.transform.localRotation = new Quaternion();
            subGameObject.transform.localScale = new Vector3(0.8f, 0.45f, 0.45f);

            VideoPlayerBaseBehaviour videoPlayer = subGameObject.GetComponent<VideoPlayerBaseBehaviour>();
            if (videoPlayer)
            {
                videoPlayer.Storage = StorageType.Absolute;
                videoPlayer.Path = video;
                videoPlayer.EnableAutoPlay = true;
                videoPlayer.EnableLoop = true;
                videoPlayer.Open();
            }
        }

        void HideObjects(Transform trans)
        {
            for (int i = 0; i < trans.childCount; ++i)
                HideObjects(trans.GetChild(i));
            if (transform != trans)
                gameObject.SetActive(false);
        }

        void ShowObjects(Transform trans)
        {
            for (int i = 0; i < trans.childCount; ++i)
                ShowObjects(trans.GetChild(i));
            if (transform != trans)
                gameObject.SetActive(true);
        }

        void ITargetEventHandler.OnTargetFound(Target target)
        {
            ShowObjects(transform);
            Debug.Log("Found: " + target.Id);
        }

        void ITargetEventHandler.OnTargetLost(Target target)
        {
            HideObjects(transform);
            Debug.Log("Lost: " + target.Id);
        }

        void ITargetEventHandler.OnTargetLoad(Target target, bool status)
        {
            Debug.Log("Load target (" + status + "): " + target.Id + " -> " + target.Name);
        }

        void ITargetEventHandler.OnTargetUnload(Target target, bool status)
        {
            Debug.Log("Unload target (" + status + "): " + target.Id + " -> " + target.Name);
        }
    }
}
