﻿/**
* Copyright (c) 2015 VisionStar Information Technology (Shanghai) Co., Ltd. All Rights Reserved.
* EasyAR is the registered trademark or trademark of VisionStar Information Technology (Shanghai) Co., Ltd in China
* and other countries for the augmented reality technology developed by VisionStar Information Technology (Shanghai) Co., Ltd.
*/

using UnityEngine;
using EasyAR;

namespace EasyARSample
{
    public class ImageTarget_DynamicLoad_ManualPlay : ImageTargetBehaviour, ITargetEventHandler
    {
        private bool loaded;
        private bool found;
        private System.EventHandler videoReayEvent;
        private VideoPlayerBaseBehaviour videoPlayer;
        private string video = "transparentvideo.mp4";

        protected override void Start()
        {
            videoReayEvent = OnVideoReady;
            base.Start();
            LoadVideo();
            HideObjects(transform);
        }

        public void LoadVideo()
        {
            GameObject subGameObject = Instantiate(Resources.Load("TransparentVideo", typeof(GameObject))) as GameObject;
            subGameObject.transform.parent = this.transform;
            subGameObject.transform.localPosition = new Vector3(0, 0.1f, 0);
            subGameObject.transform.localRotation = new Quaternion();
            subGameObject.transform.localScale = new Vector3(0.5f, 0.2f, 0.3154205f);

            videoPlayer = subGameObject.GetComponent<VideoPlayerBaseBehaviour>();
            if (videoPlayer)
            {
                videoPlayer.Storage = StorageType.Assets;
                videoPlayer.Path = video;
                videoPlayer.EnableAutoPlay = false;
                videoPlayer.EnableLoop = true;
                videoPlayer.Type = VideoPlayer.VideoType.TransparentSideBySide;
                videoPlayer.VideoReadyEvent += videoReayEvent;
                videoPlayer.Open();
            }
        }

        public void UnLoadVideo()
        {
            if (!videoPlayer)
                return;
            videoPlayer.VideoReadyEvent -= videoReayEvent;
            videoPlayer.Close();
            loaded = false;
        }

        void OnVideoReady(object sender, System.EventArgs e)
        {
            Debug.Log("Load video success");
            VideoPlayerBaseBehaviour player = sender as VideoPlayerBaseBehaviour;
            loaded = true;
            if (player && found)
                player.Play();
        }

        void HideObjects(Transform trans)
        {
            for (int i = 0; i < trans.childCount; ++i)
                HideObjects(trans.GetChild(i));
            if (transform != trans)
                gameObject.SetActive(false);
        }

        void ShowObjects(Transform trans)
        {
            for (int i = 0; i < trans.childCount; ++i)
                ShowObjects(trans.GetChild(i));
            if (transform != trans)
                gameObject.SetActive(true);
        }

        void ITargetEventHandler.OnTargetFound(Target target)
        {
            found = true;
            if (videoPlayer && loaded)
                videoPlayer.Play();
            ShowObjects(transform);
            Debug.Log("Found: " + target.Id);
        }

        void ITargetEventHandler.OnTargetLost(Target target)
        {
            found = false;
            if (videoPlayer && loaded)
                videoPlayer.Pause();
            HideObjects(transform);
            Debug.Log("Lost: " + target.Id);
        }

        void ITargetEventHandler.OnTargetLoad(Target target, bool status)
        {
            Debug.Log("Load target (" + status + "): " + target.Id + " -> " + target.Name);
        }

        void ITargetEventHandler.OnTargetUnload(Target target, bool status)
        {
            Debug.Log("Unload target (" + status + "): " + target.Id + " -> " + target.Name);
        }
    }
}
