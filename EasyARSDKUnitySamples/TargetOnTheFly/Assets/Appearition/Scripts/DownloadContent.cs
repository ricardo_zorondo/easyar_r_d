﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System.Reflection;
using UnityEngine.iOS;

public class DownloadContent : MonoBehaviour {



	Dictionary<string,string> downloadableFiles;
	Dictionary<string,string> downloadableTargets;
	string downloadfileName;
	string downloadfileUrl;
	ArrayList filesToDelete;
	public GameObject progressdial;
	ProgressBar.ProgressRadialBehaviour progressBar;
	string currentFileInPreDownload;
	bool isPredownloadInProgress=false;
	public static event Action<string> OnDownloadComplete;
	bool isDownloadComplete=false;
	bool isCurrentlyDownloading=false;
	private string cmstoken = "";
	public static string TARGET_FOLDER = "";
	public static string IMAGE_FOLDER = "";
	// Use this for initialization
	public static void SetupExternalFolders(){
	
		if (!Directory.Exists (TARGET_FOLDER)) {
			Directory.CreateDirectory(TARGET_FOLDER);
		}
		if (!Directory.Exists (IMAGE_FOLDER)) {
			Directory.CreateDirectory(IMAGE_FOLDER);
		}

	}
	void Start () {
		TARGET_FOLDER = Application.persistentDataPath + "/Targets";
		IMAGE_FOLDER = Application.persistentDataPath + "/Images";
		SetupExternalFolders ();
		Debug.Log ("DownloadContent script starting");
		#if UNITY_IOS
		Device.SetNoBackupFlag(Application.persistentDataPath+"/");
		#endif
	}

	public void StartPreDownloading(Dictionary<string,string> files,Dictionary<string,string> imageTargets, string CMSAuthorizationToken){
		/*if (progressdial == null) {
			GameObject closeb = GameObject.Find ("Canvas");
			progressdial=  closeb.transform.FindChild("ProgressRadialHollow").gameObject;

			//progressdial=GameObject.Find("ProgressRadialHollow");
		}*/
		cmstoken = CMSAuthorizationToken;
		downloadableFiles = files;
		downloadableTargets = imageTargets;
		Debug.Log ("just before progressBar called");
		progressBar = progressdial.GetComponent<ProgressBar.ProgressRadialBehaviour> ();
		Debug.Log ("progressBar called");
		StartCoroutine("PreDownloadFiles");
	}



	public void StartFileDownload(string filename,string url){
		/*if (progressdial == null) {
			GameObject closeb = GameObject.Find ("Canvas");
			progressdial=  closeb.transform.FindChild("ProgressRadialHollow").gameObject;
		}*/
		isDownloadComplete=false;
		progressBar = progressdial.GetComponent<ProgressBar.ProgressRadialBehaviour> ();
		progressdial.SetActive (true);
		//progressBar.Value = 0f;
		//progressBar.TransitoryValue = 0f;
		//progressBar.SetFillerSizeAsPercentage (0);

		isCurrentlyDownloading = true;
		downloadfileName = filename;
		downloadfileUrl = url;
		StartCoroutine (DownloadFile ());


	}



	public void DeleteDownloadedFiles(ArrayList files){

		filesToDelete = files;

		StartCoroutine("DeleteMultipleFiles");

	}







	IEnumerator DownloadFile(){
		float m_CurrentValue=0f;
		Debug.Log ("Started download" + downloadfileName);

		WWW downloadFile = new WWW(downloadfileUrl);
		while( !downloadFile.isDone ) {
			m_CurrentValue = downloadFile.progress * 100;
			progressBar.Value=m_CurrentValue;
			Debug.Log("Progress:"+m_CurrentValue);
			yield return null;
		}

		if (!string.IsNullOrEmpty(downloadFile.error)) {
			// error!
		} else {
			// success!
			Debug.Log("Download complete!");

			FileStream stream = new FileStream(IMAGE_FOLDER+ "/"+ downloadfileName, FileMode.Create);
			
			try{
				stream.Write(downloadFile.bytes, 0, downloadFile.bytes.Length);

				
				
			}
			catch(Exception ex){
				FileInfo info = new FileInfo(Application.persistentDataPath+"/"+downloadfileName);
				if (info == null || info.Exists == true) {  
					DeleteFile(IMAGE_FOLDER +"/"+downloadfileName);
					
				};
				
				Debug.Log(ex);
				yield break;
			}
			finally{
				
				stream.Close();
				stream.Dispose();
				stream = null;
			}
		}

		downloadFile.Dispose();
		downloadFile = null;
		progressBar.Value=100;
		
		while (!isDownloadComplete) {
			yield return null;
		}
		
		isDownloadComplete=false;


		if (OnDownloadComplete != null) {
			progressdial.SetActive(false);
			progressBar.Value=0f;
		    progressBar.TransitoryValue = 0f;
			isCurrentlyDownloading=false;
			//yield return new WaitForSeconds(1f);
			//progressBar.TransitoryValue = 0f;

			OnDownloadComplete (downloadfileName);
		}
		
		
		//yield return new WaitForEndOfFrame();
	


		




	}


	public void TriggerDownloadCompleted(){
		Debug.Log("DownloadComplete");
		isDownloadComplete = true;





	}


	IEnumerator InitImageTargetDownload(string filename,string url) {
		//	Debug.Log ("InitialiseAppearition started");
		Dictionary<string, string> headers = new Dictionary<string, string>();
		headers.Add("authentication-token", cmstoken);
		headers.Add ("Content-Type", "application/json");
		headers.Add("charset", "utf-8");
		Debug.Log ("imageTarget filename = " + filename + " url = " + url);
		WWW actualRequest = new WWW(url, null, headers);
	
		yield return actualRequest;
		Debug.Log ("actualRequest length" + actualRequest.bytes.Length.ToString ());
		
	
		Debug.Log ("textImage returned");
		if(actualRequest.error == null) {
			

			byte[] bytes = actualRequest.texture.EncodeToPNG();
			File.WriteAllBytes(TARGET_FOLDER +"/"+filename,bytes);

			

		} else {
			Debug.Log("targetImage download error" + actualRequest.error);
		}
	}

	IEnumerator PreDownloadFiles() {
		float m_CurrentValue=0f;
		int iFile = 0;
		string iFileName = "";
		foreach (KeyValuePair<string,string> entry in downloadableTargets) {
			m_CurrentValue = 0f;
			iFile++;
			iFileName = iFile.ToString() + ".jpg";
			Debug.Log ("Started target predownload" + entry.Key);
			//StartCoroutine(InitImageTargetDownload(entry.Key,entry.Value));
			StartCoroutine(InitImageTargetDownload(iFileName ,entry.Value));
			//m_CurrentValue = www.progress * 100;
		}
		iFile = 0;
		foreach(KeyValuePair<string,string> entry in downloadableFiles){   
			iFile++;
			iFileName = iFile.ToString() + ".jpg";
			Debug.Log ("Started media predownload" + entry.Key);
			m_CurrentValue = 0f;
			currentFileInPreDownload = iFileName; //entry.Key;
			WWW www = new WWW(entry.Value);
			Debug.Log ("Downloading:..." + entry.Value);
			while( !www.isDone ) {
				//if(isPredownloadInProgress){
				m_CurrentValue = www.progress * 100;
			//	progressBar.Value=m_CurrentValue;
				//Debug.Log("Progress:"+m_CurrentValue);	
				//}
				yield return null;
			}


			if (!string.IsNullOrEmpty(www.error)) {
				// error!
				Debug.Log("Download error");
			}
			else{
				// success!
				Debug.Log("Download complete!");


				//FileStream stream = new FileStream(IMAGE_FOLDER + "/"+ entry.Key, FileMode.Create);
				FileStream stream = new FileStream(IMAGE_FOLDER + "/"+ iFileName , FileMode.Create);
				
				try{
					Debug.Log("Download File size:::"+www.bytes.Length);
					stream.Write(www.bytes, 0, www.bytes.Length);
					if(isPredownloadInProgress){
						progressBar.Value=100;
						progressdial.SetActive(false);
						if(OnDownloadComplete != null){
							progressBar.Value=0f;
							progressBar.TransitoryValue = 0f;
							//OnDownloadComplete(downloadfileName);
							OnDownloadComplete(iFileName);
						}

					}


				}
				catch(Exception ex){

					//FileInfo info = new FileInfo(IMAGE_FOLDER+"/"+entry.Key);
					FileInfo info = new FileInfo(IMAGE_FOLDER+"/"+iFileName);
					if (info == null || info.Exists == true) {  
						//DeleteFile(IMAGE_FOLDER +"/"+entry.Key);
						DeleteFile(IMAGE_FOLDER +"/"+iFileName);
						
					};
					
					Debug.LogError(ex.Message);
					yield break;
				}
				finally{


					stream.Close();
					stream.Dispose();
					stream = null;
				}


			}

			www.Dispose();
			www = null;

		}
	}

	public void CheckPreDownloadStatus(string file,string url){
		isPredownloadInProgress = false;

		// If the file is already in the process of downloading as set by predownload just show the progress
		if (file == currentFileInPreDownload) {
			Debug.Log ("File is already in the process of downloading as set by predownload just show the progress");
			isPredownloadInProgress=true;
			progressdial.SetActive (true);
			progressBar = progressdial.GetComponent<ProgressBar.ProgressRadialBehaviour> ();

			/*float m_CurrentValue=0f;
			progressBar = progressdial.GetComponent<ProgressBar.ProgressRadialBehaviour> ();
			progressdial.SetActive (true);
			while( !www.isDone ) {
				m_CurrentValue = www.progress * 100;
				progressBar.Value=m_CurrentValue;
				Debug.Log("Progress:"+m_CurrentValue);
			}

			progressdial.SetActive(false);

*/

		} 
		//if the file is set as predownload but in the queuo to be downloaded remove it from the queue and download separately.
		else {
			downloadableFiles.Remove(file);
			StartFileDownload(file,url);

		}



	}



	public IEnumerator DeleteMultipleFiles(){
		Debug.Log ("Deleteing multiple files");
		foreach (string file in filesToDelete) {
			try{
				if(file.Contains(".zip")){
					DirectoryInfo dir = new DirectoryInfo(IMAGE_FOLDER+"/"+file.Substring(0,file.Length-4));
					if (dir != null & dir.Exists) {
						
						Directory.Delete(IMAGE_FOLDER +"/"+file.Substring(0,file.Length-4),true);
					}
				}

				File.Delete (IMAGE_FOLDER+ "/"+file);

			}
			catch(Exception e){
				Debug.Log(e);
				
			}

		}
		yield return null;


	}


	public void DeleteFile(string path){
		try{
		File.Delete (path);
		}
		catch(Exception e){
			Debug.Log(e);

		}
	}



	public void CancelDownload(){

		if (isCurrentlyDownloading) {

			progressdial.SetActive(false);
			progressBar.Value=0f;
			progressBar.TransitoryValue = 0f;



		}


	}


}
