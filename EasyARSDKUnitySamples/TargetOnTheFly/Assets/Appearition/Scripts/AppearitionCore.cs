﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine.EventSystems;
using UnityEngine.Events;
//using IndieYP;
using UnityEngine.UI;



public class AppearitionCore : MonoBehaviour {
	
	public Material standardMaterial;
	public Material transparentMaterial;
	public string CMSAuthorizationToken="";
	public string CMSEndPoint="";
	AssetCollection allAssets;
//	CraftARItem foundItem;
	private GameObject _webView;
	Dictionary<string,string> filesToDownload;
	Dictionary<string,string> TargetImagesToDownload;

	ArrayList alreadyDownloadedFiles;
	ArrayList filesToDelete;
	public GameObject scanline;
	public static Asset detectedAsset;
//	MediaPlayerCtrl scrMedia=null;
	public GameObject closeButton;
//	GoogleAnalyticsV3 googleAnalytics; 
	private int unzipprogress;
	public Button scanAgainButton;
	public float scanTimer;
	public Material ChromaKeyMaterial;
	//public static bool ScanStarted = false;
	
	void OnEnable ()
	{
		DownloadContent.OnDownloadComplete += DownloadComplete;
		
	}
	
	void OnDisable ()
	{
		
		DownloadContent.OnDownloadComplete -= DownloadComplete;
	}
	
	void Start() {
//		googleAnalytics = GetComponent<GoogleAnalyticsV3> ();
//		googleAnalytics.StartSession ();
		StartCoroutine(InitialiseAppearition());
		filesToDownload = new Dictionary<string,string> ();
		TargetImagesToDownload = new Dictionary<string,string> ();
		filesToDelete = new ArrayList ();
		alreadyDownloadedFiles = new ArrayList ();
		//Invoke ("StartScanCounter",scanTimer);
	}
	
/*	
	void InitializeWebView(){
		_webView = new GameObject ();
		_webView.AddComponent<UniWebView>();
		_webView.GetComponent<UniWebView>().OnLoadComplete += OnLoadComplete;
		_webView.GetComponent<UniWebView>().OnWebViewShouldClose += OnWebViewShouldClose;
		_webView.GetComponent<UniWebView>().ShowToolBar(true);
			

		
		
	} */

/*	void StartScanCounter(){

		InvokeRepeating ("StartScanCounterRepeating", 0, scanTimer);

	} */


/*	void StartScanCounterRepeating(){
		if (!CatchoomAR.hasStartedRendering) {

		//	StopScanningMode();
		//	StopScanCounter();
		//	googleAnalytics.LogEvent(ApplicationConstants.GA_CATEGORY_SCAN,ApplicationConstants.GA_ACTION_NOT_FOUND,"",0);
		//	scanAgainButton.gameObject.SetActive(true);

		}



	} */


	/*void ShowCloseButton(){
		closeButton.SetActive(true);
	} */


	
	public void DownloadComplete(string fileName){
		Debug.Log ("Download complete: "+fileName);
		//HandleRendering ();
		
		
	}
	
	private WWW GetCMSLoader(string url) {
		Dictionary<string, string> headers = new Dictionary<string, string>();
		headers.Add("authentication-token", CMSAuthorizationToken);
		headers.Add ("Content-Type", "application/json");
		headers.Add("charset", "utf-8");
		WWW actualRequest = new WWW(url, null, headers);

		return actualRequest;
	}
	
	IEnumerator InitialiseAppearition() {
	//	Debug.Log ("InitialiseAppearition started");
		WWW actualRequest = GetCMSLoader(CMSEndPoint);

		yield return actualRequest;
//Debug.Log ("InitialiseAppearition returned");
		if(actualRequest.error == null) {

			allAssets = new AssetCollection(actualRequest.text);
			HandleFileContets();
		} else {
			Debug.Log(actualRequest.error);
		}
	}

	
	void HandleFileContets(){
		//Debug.Log("HandleFileContets");
		if (allAssets == null) {
			Debug.Log("No files");
			return;
		}
		Asset[] assets = allAssets.getAssets();

		int iMedia = 0;
		          
		foreach(Asset asset in assets ){
			Debug.Log ("url = " + assets [0].mediaFiles[0].mediaUrl.ToString());
			Debug.Log ("filename = " + assets [0].mediaFiles [0].fileName.ToString ());
			Debug.Log ("targetImage = " + assets [0].targetImages[0].fileName.ToString());

			MediaFile item = null;
			targetImage targetItem = null;
			if(asset.mediaFiles.Length>0 && asset.targetImages.Length > 0){
				//check for platform or type and choose correct media in future.
				item= asset.mediaFiles[0];
				targetItem = asset.targetImages[0];

			}
			if(item!=null && item.fileName!=null && !"null".Equals(item.fileName)
	 && targetItem!=null && targetItem.fileName!=null && !"null".Equals(targetItem.fileName)){

				FileInfo info = new FileInfo(Application.persistentDataPath+"/"+item.fileName);
			//	FileInfo info2 = new FileInfo(Application.persistentDataPath+"/"+targetItem.fileName);
				filesToDownload.Add(item.fileName,item.mediaUrl);
				TargetImagesToDownload.Add (targetItem.fileName,targetItem.mediaUrl);
			//	Debug.Log ("Just before InitImageTargetDownload: " + targetItem.fileName + "|" + targetItem.mediaUrl);

			//	Debug.Log ("Just after InitImageTargetDownload: " + targetItem.fileName + "|" + targetItem.mediaUrl);
			//	filesToDownload.Add(targetItem.fileName,targetItem.mediaUrl);


		/*	if(item.isPreDownload){
				if(info.Exists==false){
						filesToDownload.Add(item.fileName,item.mediaUrl);
				}else{
						alreadyDownloadedFiles.Add(item.fileName);
					//Debug.Log("Predownload is true and file already exists");
				}
			}else{
				if(info.Exists==true){
					//Debug.Log("Predownload is false and file already exists");
						if(!alreadyDownloadedFiles.Contains(item.fileName)){
							filesToDelete.Add(item.fileName);
					}
				}
			} */
				iMedia++;
			DirectoryInfo dir = new DirectoryInfo(Application.persistentDataPath);

			FileInfo[] fileInfo = dir.GetFiles();
			foreach (FileInfo f in fileInfo) 
			{ 
				
				Asset a = allAssets.SearchForMediaName(f.Name);

				if(a==null){
					
					filesToDelete.Add(f.Name);
				}
				
				
			}
		}
			
		}
		
		
		
		if (filesToDownload.Count > 0 && TargetImagesToDownload.Count > 0) {
			Debug.Log ("Just before download");
			GetComponent<DownloadContent> ().StartPreDownloading (filesToDownload,TargetImagesToDownload,CMSAuthorizationToken);
		}
		if (filesToDelete.Count > 0) {
			
//			GetComponent<DownloadContent>().DeleteDownloadedFiles(filesToDelete);
			
		}
		

		
	}
	
/*	public void DetectedByAREngine(CraftARItem  bestMatch){
	//	googleAnalytics.LogEvent(ApplicationConstants.GA_CATEGORY_SCAN,ApplicationConstants.GA_ACTION_FOUND,bestMatch.itemName,0);
		StopScanCounter();
		if(allAssets == null) {
			Debug.Log("All assets is null");
	//		CraftARSDK.instance.startFinderMode();
			StartScanCountdown();
			return;
		}
		scanline.SetActive (false);
	//	foundItem = null;
	//	foundItem = bestMatch;
	//	detectedAsset = allAssets.SearchByID(foundItem.itemName);

		if (detectedAsset != null && detectedAsset.mediaFiles[0]!=null) {
			 

			googleAnalytics.LogEvent(ApplicationConstants.GA_CATEGORY_SCAN,ApplicationConstants.GA_ACTION_MATCHING_ASSET_FOUND,detectedAsset.assetId,0);
			if(detectedAsset.mediaFiles[0].mediaType==ApplicationConstants.MEDIA_TYPE_WEBLINK||detectedAsset.mediaFiles[0].mediaType==ApplicationConstants.MEDIA_TYPE_VIDEO_STREAM||detectedAsset.mediaFiles[0].mediaType==ApplicationConstants.MEDIA_TYPE_PDF){
				
				HandleRendering();
				
			}

			else{
				FileInfo info = new FileInfo(Application.persistentDataPath+"/"+detectedAsset.mediaFiles[0].fileName);
			if(info!=null && info.Exists==true){
				HandleRendering ();
			}else{
					if(detectedAsset.mediaFiles[0].isPreDownload){
						GetComponent<DownloadContent>().CheckPreDownloadStatus(detectedAsset.mediaFiles[0].fileName,detectedAsset.mediaFiles[0].mediaUrl);
					
				}else{
						GetComponent<DownloadContent>().StartFileDownload(detectedAsset.mediaFiles[0].fileName,detectedAsset.mediaFiles[0].mediaUrl);
				}
			} 


			}
			
		} else {
			//No Asset returned from CMS which has been identified by AR SDK
			
			Debug.Log("No Asset returned from CMS which has been identified by AR SDK");
			googleAnalytics.LogEvent(ApplicationConstants.GA_CATEGORY_SCAN,ApplicationConstants.GA_ACTION_NO_MATCHING_ASSET,foundItem.itemName,0);
			NativeToolkit.ShowAlert("No images found.","Try pointing to the object or area whilebeing closer or from another angle.",NoAssetsAlert);
			return;
			
		}
		
		
	} */

/*	void NoAssetsAlert(bool result){

		StartSearchingAgain();
	}
	*/
	
/*	public void TrackerInFrame(){
		if (scrMedia == null) {
			
			return;
		} else {
			
			scrMedia.Play();
		}
		
		
		
	} */
	
	
/*	public void TrakerLost(){
		if (scrMedia == null) {
			
			return;
		} else {
			
			scrMedia.Pause();
		}
		
		
		
	} */

	/*
	IEnumerator PlayVideoCoroutine(string videoPath)
	{

		Debug.Log("Playing fullscreen Video");
		Handheld.PlayFullScreenMovie(videoPath, Color.black,FullScreenMovieControlMode.Full, FullScreenMovieScalingMode.None);    
		//Skip two frames as per the doocs and forums
		yield return new WaitForEndOfFrame();
		yield return new WaitForEndOfFrame();
		Debug.Log("Video playback completed.");
		StopScanCounter ();
		StartSearchingAgain ();
	}
	
	private void HandleRendering(){
		googleAnalytics.LogEvent(ApplicationConstants.GA_CATEGORY_PLAY_MEDIA,detectedAsset.mediaFiles[0].mediaType,detectedAsset.assetId,0);
		Debug.Log ("HandleRendering of item name:"+foundItem.itemName);
		scrMedia = null;
		if (detectedAsset.mediaFiles[0].mediaType == ApplicationConstants.MEDIA_TYPE_VIDEO) {
			if (detectedAsset.mediaFiles[0].isTracking) {

				ShowCloseButton ();
				
				bool itemAdded = CraftARSDK.instance.AddSceneARItem (foundItem, true);
				if (itemAdded) {
					if (foundItem.contentInstance != null) {
						Debug.Log ("FoundItemName:" + foundItem.contentInstance.name);
					}
					
					Debug.Log ("Adding video on target");
					CraftARSDK.instance.startTracking ();
					GameObject content = Resources.Load ("ARItem_VideoOnTarget", typeof(GameObject)) as GameObject;  
					Vector3 position = new Vector3 (detectedAsset.mediaFiles[0].translation.x, detectedAsset.mediaFiles[0].translation.y, detectedAsset.mediaFiles[0].translation.z);
					Vector3 rotation = new Vector3 (90 + detectedAsset.mediaFiles[0].rotation.x, detectedAsset.mediaFiles[0].rotation.y, detectedAsset.mediaFiles[0].rotation.z);
					Vector3 scale = new Vector3 (detectedAsset.mediaFiles[0].scale.x, detectedAsset.mediaFiles[0].scale.y, detectedAsset.mediaFiles[0].scale.z);
					foundItem.contentInstance = GameObject.Instantiate (content, content.transform.position, content.transform.rotation) as GameObject;						


					GameObject videoManager = foundItem.contentInstance.transform.FindChild ("VideoManager").gameObject;
					if (videoManager != null) {
						videoManager.transform.localPosition = position; 
						videoManager.transform.localEulerAngles = rotation;
						videoManager.transform.localScale = scale; 
					}

					scrMedia = foundItem.contentInstance.GetComponentInChildren<MediaPlayerCtrl> ();
					scrMedia.Load ("file://" + Application.persistentDataPath + "/" + detectedAsset.mediaFiles[0].fileName);
					scrMedia.Play ();
				} else {

					Debug.Log ("Item not added as its already in the list");
				}
				
			} else {
				StartCoroutine (PlayVideoCoroutine ("file://" + Application.persistentDataPath + "/" + detectedAsset.mediaFiles[0].fileName));
				
			}
			
			
			
			
			
		} else if (detectedAsset.mediaFiles[0].mediaType == ApplicationConstants.MEDIA_TYPE_TRANSPARENT_VIDEO) {
			//Handle Transparent Video rendering using chromakey material

			ShowCloseButton ();
			
			bool itemAdded = CraftARSDK.instance.AddSceneARItem (foundItem, true);
			if (itemAdded) {
				if (foundItem.contentInstance != null) {
					Debug.Log ("FoundItemName:" + foundItem.contentInstance.name);
				}
				
				Debug.Log ("Adding video on target");
				CraftARSDK.instance.startTracking ();
				GameObject content = Resources.Load ("ARItem_VideoOnTarget", typeof(GameObject)) as GameObject;  
				Vector3 position = new Vector3 (detectedAsset.mediaFiles[0].translation.x, detectedAsset.mediaFiles[0].translation.y, detectedAsset.mediaFiles[0].translation.z);
				Vector3 rotation = new Vector3 (90 + detectedAsset.mediaFiles[0].rotation.x, detectedAsset.mediaFiles[0].rotation.y, detectedAsset.mediaFiles[0].rotation.z);
				Vector3 scale = new Vector3 (detectedAsset.mediaFiles[0].scale.x, detectedAsset.mediaFiles[0].scale.y, detectedAsset.mediaFiles[0].scale.z);
				foundItem.contentInstance = GameObject.Instantiate (content, content.transform.position, content.transform.rotation) as GameObject;						
				
				
				GameObject videoManager = foundItem.contentInstance.transform.FindChild ("VideoManager").gameObject;
				if (videoManager != null) {
					videoManager.transform.localPosition = position; 
					videoManager.transform.localEulerAngles = rotation;
					videoManager.transform.localScale = scale; 
				}
				videoManager.GetComponent<Renderer>().material = ChromaKeyMaterial;
				scrMedia = foundItem.contentInstance.GetComponentInChildren<MediaPlayerCtrl> ();
				scrMedia.Load ("file://" + Application.persistentDataPath + "/" + detectedAsset.mediaFiles[0].fileName);
				scrMedia.Play ();
			} else {
				
				Debug.Log ("Item not added as its already in the list");
			}


		}

		
		else if(detectedAsset.mediaFiles[0].mediaType==ApplicationConstants.MEDIA_TYPE_VIDEO_STREAM){
			InitializeWebView();
			Debug.Log(detectedAsset.mediaFiles[0].mediaUrl);

			_webView.GetComponent<UniWebView>().Show();
			String htmlString = " <html><head></head><body style=\"margin:0px;padding:0px;overflow:hidden\"><iframe width=\"100%\" height=";
			htmlString+="\"100%\" ";
			htmlString+="src=\"" +
				detectedAsset.mediaFiles[0].mediaUrl					
					+"\" frameborder=\"0\" style=\"overflow:hidden;height:100%;width:100%\"></iframe></body></html>";
				

			_webView.GetComponent<UniWebView>().LoadHTMLString(htmlString,"");
			_webView.GetComponent<UniWebView>().SetSpinnerLabelText("Please Wait");
			_webView.GetComponent<UniWebView>().SetShowSpinnerWhenLoading(true);

			//_webView.Load();
			
			
			
		}
		else if(detectedAsset.mediaFiles[0].mediaType==ApplicationConstants.MEDIA_TYPE_WEBLINK){
			InitializeWebView();
			Debug.Log(detectedAsset.mediaFiles[0].mediaUrl);
			_webView.GetComponent<UniWebView>().url=detectedAsset.mediaFiles[0].mediaUrl;
			//_webView.GetComponent<UniWebView>().backButtonEnable=false;
			_webView.GetComponent<UniWebView>().Show();
			_webView.GetComponent<UniWebView>().Load();
			
		}else if(detectedAsset.mediaFiles[0].mediaType==ApplicationConstants.MEDIA_TYPE_3D)
		{
			// Media type 3d currently reads only OBJ files in zip format with obj, mtl and png texture(Note: texture needs to be in png format only)


			StartCoroutine(Load3DObject());

			
		}
		else if(detectedAsset.mediaFiles[0].mediaType==ApplicationConstants.MEDIA_TYPE_UNITY_ANDROID){
			Debug.Log("Detected as unity_android");
			StartCoroutine(LoadAsset(detectedAsset));
		}
		
		else if(detectedAsset.mediaFiles[0].mediaType==ApplicationConstants.MEDIA_TYPE_UNITY_IOS){
			Debug.Log("Detected as unity_ios");

			StartCoroutine(LoadAsset(detectedAsset));
			
		}
		
		else if(detectedAsset.mediaFiles[0].mediaType==ApplicationConstants.MEDIA_TYPE_ANIMATION){
			
			
		}
		
		else if(detectedAsset.mediaFiles[0].mediaType==ApplicationConstants.MEDIA_TYPE_PANAROMA){
			
			
		}
		
		else if(detectedAsset.mediaFiles[0].mediaType==ApplicationConstants.MEDIA_TYPE_PDF){
			// works on iOS using Uniwebview check if it works on Android as the webkit in android cannot read pdf

			#if UNITY_IOS
			// InitializeWebView();
		//	Debug.Log(detectedAsset.mediaUrl);
		//	_webView.url="file://"+Application.persistentDataPath+"/"+detectedAsset.mediaName;
		//	_webView.Show();
		//	_webView.Load();


		//	InitializeWebView();
		//	Debug.Log(detectedAsset.mediaFiles[0].mediaUrl);
		//	_webView.url=detectedAsset.mediaFiles[0].mediaUrl;
		//	_webView.Show();
		//	_webView.Load();


			InitializeWebView();
			Debug.Log(detectedAsset.mediaFiles[0].mediaUrl);
			_webView.GetComponent<UniWebView>().url=detectedAsset.mediaFiles[0].mediaUrl;
			_webView.GetComponent<UniWebView>().Show();
			_webView.GetComponent<UniWebView>().Load();



			#endif
			#if UNITY_ANDROID
			ShowCloseButton();
			PDFReader.OpenDocRemote(detectedAsset.mediaFiles[0].mediaUrl, true);
			//PDFReader.OpenDocLocalAndroid(Application.persistentDataPath+"/"+detectedAsset.mediaName);
			//ShowCloseButton();
			//Application.OpenURL(Application.persistentDataPath+"/"+detectedAsset.mediaName);

			#endif

		
		}
		
		else if(detectedAsset.mediaFiles[0].mediaType==ApplicationConstants.MEDIA_TYPE_IMAGE){

			StartCoroutine(ShowImageOnTarget());
		}
		
	}


*//*

	IEnumerator ShowImageOnTarget(){

		ShowCloseButton();
		
		bool itemAdded = CraftARSDK.instance.AddSceneARItem(foundItem,true);
		if (itemAdded) {
			if(foundItem.contentInstance!=null){
				Debug.Log ("FoundItemName:" + foundItem.contentInstance.name);
			}
			
			Debug.Log("Adding image on target");
			CraftARSDK.instance.startTracking ();
			GameObject content = Resources.Load ("ARItem_Image",typeof(GameObject)) as GameObject;  
			Vector3 position= new Vector3(detectedAsset.mediaFiles[0].translation.x,detectedAsset.mediaFiles[0].translation.y,detectedAsset.mediaFiles[0].translation.z);
			Vector3 rotation =  new Vector3(detectedAsset.mediaFiles[0].rotation.x,180f+detectedAsset.mediaFiles[0].rotation.y,detectedAsset.mediaFiles[0].rotation.z);
			Vector3 scale =  new Vector3(detectedAsset.mediaFiles[0].scale.x,0,detectedAsset.mediaFiles[0].scale.z);
			
			
			
			foundItem.contentInstance =  GameObject.Instantiate(content,content.transform.position, content.transform.rotation) as GameObject;						
			
			
			GameObject imageOnTarget=  foundItem.contentInstance.transform.FindChild("Image").gameObject;
			
			imageOnTarget.GetComponent<Renderer>().material.mainTexture = new Texture2D(4, 4, TextureFormat.RGBA32, false);
			WWW imagePath = new WWW("file://" + Application.persistentDataPath+"/"+ detectedAsset.mediaFiles[0].fileName);
			
			yield return imagePath;

			imagePath.LoadImageIntoTexture(imageOnTarget.GetComponent<Renderer>().material.mainTexture as Texture2D);
			
			imagePath.Dispose();
			imagePath = null;
			
			if(imageOnTarget!=null){
				imageOnTarget.transform.localPosition=position;
				imageOnTarget.transform.localEulerAngles=rotation;
				imageOnTarget.transform.localScale=scale;

			}

		}



	}
	
	
	void OnLoadComplete(UniWebView webView, bool success, string errorMessage) {
		
		if (success) {
			webView.Show();
		} else {
			Debug.Log("Something wrong in webview loading: " + errorMessage);
			
		}
	}
	
	bool OnWebViewShouldClose(UniWebView webView) {
		//currentlyLoaded = false;
		Debug.Log ("Called on OnWebViewShouldClose");
		StartSearchingAgain();
		return true;
		//if (_webView.GetComponent<UniWebView>()) {
		//	_webView = null;
		//	StartSearchingAgain();
		//	return true;
	//	}
	//	return false;
	}

	public void ScanClean(){
		Debug.Log("StartSearchingAgain");
		
		
		
		if (scrMedia != null) {
			
			scrMedia.Stop();
			Debug.Log("Media stopped");
			
		}
		if (_webView != null) {
			
			_webView.GetComponent<UniWebView>().Hide();
			_webView.GetComponent<UniWebView>().OnLoadComplete -= OnLoadComplete;
			_webView.GetComponent<UniWebView>().OnWebViewShouldClose -= OnWebViewShouldClose;
			DestroyImmediate(_webView);
			
		}
		
		closeButton.SetActive (false);
		Debug.Log("Tracking 1");
		CraftARSDK.instance.stopTracking ();
		Debug.Log("Tracking stopped");
	}
	*/
	
/*	public void StartSearchingAgain(){
	
		ScanClean ();

		StartCoroutine (UnLoadAssets());

		
	}
	public void CloseMedia(){
		StopScanCounter();
		StopScanningMode ();
		ScanClean ();
		
		StartCoroutine (UnLoadAssetsNoScan());
	}



	public void StopScanningMode(){
		Debug.Log("StopScanningMode");
		
		if (scrMedia != null) {
			scrMedia.Stop();
			
		}
		scanline.SetActive (false);
		closeButton.SetActive (false);
		if (CraftARSDK.instance.isActiveAndEnabled) {
			Debug.Log("Tracking 2");
			CraftARSDK.instance.stopTracking ();
			Debug.Log("past Tracking 2");
			CraftARSDK.instance.stopFinderMode ();
			if (foundItem != null) {
				CraftARSDK.instance.RemoveARItem (foundItem);
				DestroyImmediate (foundItem.content);
				DestroyImmediate (foundItem.contentInstance);
				Debug.Log ("DestroyImmediate foundItem.contentInstance");
				DestroyImmediate (foundItem);
				Debug.Log ("DestroyImmediate foundItem");
			}
		}
		
	}


	IEnumerator UnLoadAssetsNoScan(){
		
		yield return new WaitForEndOfFrame ();
		
		//CraftARSDK.instance.removeARItem(foundItem);
		Debug.Log("removeARItem foundItem");
		if (foundItem != null) {
			CraftARSDK.instance.RemoveARItem (foundItem);
			Debug.Log ("RemoveARItem foundItem");
			DestroyImmediate (foundItem.content);
			Debug.Log ("DestroyImmediate foundItem.conten");
			DestroyImmediate (foundItem.contentInstance);
			Debug.Log ("DestroyImmediate foundItem.contentInstanc");
			DestroyImmediate (foundItem);
			Debug.Log ("DestroyImmediate foundItem");
		}
		
		//Wait for things to settle down as the user might still be pointing at the image so it that it wont recognise immediately.
		yield return new WaitForSeconds (3f);
		

		
		
		
	}


	IEnumerator UnLoadAssets(){

		yield return new WaitForEndOfFrame ();

		//CraftARSDK.instance.removeARItem(foundItem);
		Debug.Log("removeARItem foundItem");
		if (foundItem != null) {
			CraftARSDK.instance.RemoveARItem (foundItem);
			Debug.Log ("RemoveARItem foundItem");
			DestroyImmediate (foundItem.content);
			Debug.Log ("DestroyImmediate foundItem.conten");
			DestroyImmediate (foundItem.contentInstance);
			Debug.Log ("DestroyImmediate foundItem.contentInstanc");
			DestroyImmediate (foundItem);
			Debug.Log ("DestroyImmediate foundItem");
		}

		//Wait for things to settle down as the user might still be pointing at the image so it that it wont recognise immediately.
		yield return new WaitForSeconds (3f);
    	
		StartScanner ();



	}


	private void StartScanner(){

		CatchoomAR.hasStartedRendering=false;
		CraftARSDK.instance.startFinderMode ();
		StartScanCountdown ();
		scanline.SetActive (true);

	}

	
	
	IEnumerator LoadAsset(Asset detected){
		Debug.Log("In LoadAsset");
		FileInfo info = new FileInfo(Application.persistentDataPath+"/"+detectedAsset.mediaFiles[0].fileName);
		if(info!=null && info.Exists==true){
			using (WWW bundleDL = new WWW("file://" + Application.persistentDataPath+"/"+ detectedAsset.mediaFiles[0].fileName)) {
				yield return bundleDL;
				GameObject instantiated = null;
				foreach(UnityEngine.Object obj in bundleDL.assetBundle.LoadAllAssets()) {
					if(obj.GetType() == typeof(GameObject)) {
						instantiated = (GameObject)Instantiate(obj);
						break;
					}
				}
				if(instantiated != null) {
					yield return StartCoroutine(RunAsset(instantiated));
					Destroy(instantiated);
				}
				yield return null;
				bundleDL.assetBundle.Unload(true);
			}
			
		}else{
			
			
			
		}
		
	}


	IEnumerator Load3DObject(){

		ShowCloseButton ();

		string zipfilePath = Application.persistentDataPath+ "/"+ detectedAsset.mediaFiles[0].fileName;
		string exportLocation = Application.persistentDataPath+ "/";
		lzip.decompress_File(zipfilePath,exportLocation,ref unzipprogress);
		Debug.Log("Unzip Successful");

		string objFile = "";
		
		DirectoryInfo dir = new DirectoryInfo(Application.persistentDataPath+"/"+detectedAsset.mediaFiles[0].fileName.Substring(0,detectedAsset.mediaFiles[0].fileName.Length-4));

		FileInfo[] info = dir.GetFiles("*.obj");

		foreach (FileInfo f in info) 
		{ 
			objFile = f.Name;
		}

		Debug.Log ("Load3DObject:path::" + objFile);
		GameObject[] threeDObject = ObjReader.use.ConvertFile (Application.persistentDataPath+"/"+detectedAsset.mediaFiles[0].fileName.Substring(0,detectedAsset.mediaFiles[0].fileName.Length-4)+"/"+objFile, true, standardMaterial, transparentMaterial);

	
		if (threeDObject != null && threeDObject.Length > 0) {

			bool itemAdded = CraftARSDK.instance.AddSceneARItem (foundItem, true);
			if (itemAdded) {
				Debug.Log("Adding 3D obj on target");
				CraftARSDK.instance.startTracking ();
				GameObject content = Resources.Load ("ARItem_3DContent",typeof(GameObject)) as GameObject;  
				Vector3 position= new Vector3(detectedAsset.mediaFiles[0].translation.x,detectedAsset.mediaFiles[0].translation.y,detectedAsset.mediaFiles[0].translation.z);
				Vector3 rotation =  new Vector3(detectedAsset.mediaFiles[0].rotation.x,detectedAsset.mediaFiles[0].rotation.y,detectedAsset.mediaFiles[0].rotation.z);
				Vector3 scale = new Vector3(detectedAsset.mediaFiles[0].scale.x,detectedAsset.mediaFiles[0].scale.y,detectedAsset.mediaFiles[0].scale.z);
				foundItem.contentInstance =  GameObject.Instantiate(content,content.transform.position, content.transform.rotation) as GameObject;						
				GameObject parentContent=  foundItem.contentInstance.transform.FindChild("content").gameObject;


				if(parentContent!=null){
					for(int i=0;i<threeDObject.Length;i++){
					threeDObject[0].transform.parent=parentContent.transform;
					}
					parentContent.transform.localPosition=position;
					parentContent.transform.localEulerAngles=rotation;
					parentContent.transform.localScale=scale;
				}

			}
		}
		
		yield return null;
	}


	public void StopScanCounter(){

		CancelInvoke ("StartScanCounter");
		CancelInvoke ("StartScanCounterRepeating");

	}



	public void StartScanCountdown(){
		
		
		Invoke ("StartScanCounter",scanTimer);
		
	}

	
	IEnumerator RunAsset(GameObject loaded) {
		bool itemAdded = CraftARSDK.instance.AddSceneARItem(foundItem,true);
		if (itemAdded) {
			CraftARSDK.instance.startTracking ();
			foundItem.contentInstance =  GameObject.Instantiate(loaded,loaded.transform.position, loaded.transform.rotation) as GameObject;
			
			
		}
		yield return null;
	}
	
	*/
}
