﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class MyGrid : MonoBehaviour {
	public int selGridInt = 0;
	public int leftofGrid = 25;
	public int topofGrid = 25;
	Dictionary<string,Texture2D> textureDict;
	Dictionary<string,GUIContent> guiContentDict;
	int ScreenWidth =0;
	int ScreenHeight = 0;
	public bool DrawTable = true;
	
	// Use this for initialization
	public string[] selStrings = new string[] {"Cell 1", "Cell 2", "Cell 3", "Cell 4", "Cell 5", "Cell 6","Cell 7", "Cell 8", "Cell 9"};
	public int width = 300;
	public int height = 100;
	public int cols = 3;
	public int ImageRectSize = 64;
	static int TEXT = 0;
	static int IMAGE = 1;
	public Texture2D tex;
	GUIContent[] guiArray;
	Vector2 scrollPosition;
	GUIStyle mCellStyle;
	Texture2D whiteTexture;
	public int TableWidthPercent = 100;
	public int TableHeightPercent = 100;
	public int RowViewPortNumber = 5;
	public int ImageWidthPercent = 15;
	public int DescriptionWidthPercent = 80;

	private int actualWidth = 0;
	private int actualHeight = 0;
	private float LineWidth = 1.0f;
	public Color LineColor = Color.black;
	public Texture2D lineTex;
	public Texture2D buttonTex;
	public int GridRowHeight = 40;
	public Font buttonFont;
	public Font textFont;
	public int RowIndex;
	private int ActualImageWidth = 0;
	private int ActualDescriptionWidth = 0;
	private int ActualButtonWidth = 0;
	GUIContent Description = null;
	GUIContent ButtonName = null;
	GUIContent image = null;

	private void SetTableDims(){
		int rowheight = 0;
		DescriptionWidthPercent = 55;
		actualWidth = (Screen.width/100) * TableWidthPercent;
		actualHeight = (Screen.height/100) * TableHeightPercent;
	
		GridRowHeight = (actualHeight/RowViewPortNumber);
		ActualImageWidth = GridRowHeight;
	//	ActualButtonWidth = (Screen.width/100) * ButtonWidthPercent;
		ActualDescriptionWidth = (Screen.width/100) * DescriptionWidthPercent;




	}
	
	
	void Start () {
		SetTableDims ();
		textureDict = new Dictionary<string, Texture2D>();
		guiContentDict = new Dictionary<string,GUIContent > ();
		whiteTexture = Resources.Load<Texture2D>("white");
		lineTex = Resources.Load <Texture2D>("black");
		buttonTex = Resources.Load<Texture2D> ("SCCButtonTex");
		buttonFont = Font.CreateDynamicFontFromOSFont ("Roboto-Bold", 24);
		textFont = Font.CreateDynamicFontFromOSFont ("Arial", 24);
	

/*		mCellStyle = new GUIStyle();//style for cells
		mCellStyle.normal.background = Resources.Load<Texture2D>("Textures/button_up_9patch");
		mCellStyle.onNormal.background = Resources.Load<Texture2D>("Textures/button_down_9patch");
		mCellStyle.focused.background = mButtonStyle.active.background;
		mCellStyle.fontSize = GUIUtils.GetKegel() - GUIUtils.GetKegel() / 5;
		mCellStyle.border = new RectOffset(7, 7, 7, 7);
		mCellStyle.padding = new RectOffset(20, 20, 20, 20);
		mCellStyle.alignment = TextAnchor.MiddleCenter;
		mCellStyle.wordWrap = true; */
		TestArray ();
	}
	
	// Update is called once per frame
	void Update () {
		if(DrawTable)
			SetTableDims ();
	}
	private IEnumerator UrlToSprite(string index,string imageUrl){
		
		WWW imagePath = new WWW(imageUrl);
		yield return imagePath;
		Sprite sprite = null;
		sprite = Sprite.Create(imagePath.texture, new Rect(0, 0, ImageRectSize, ImageRectSize), new Vector2(0.5f, 0.5f));
		if (sprite != null) {
			Debug.Log ("sprite:" + imagePath.url.ToString () + " was created"); 
		} else {
			Debug.Log ("sprite: Creation Problem with " + imagePath.url.ToString ());
		}
		
		imagePath.Dispose();
		imagePath = null;
		
		
	}
	private IEnumerator UrlToTexture(string index,string imageUrl,int type){
		
		WWW imagePath = new WWW(imageUrl);
		yield return imagePath;
		if (type.Equals (TEXT)) {
			guiContentDict.Add (index.ToString(), new GUIContent (imagePath.text));
		}else if(type.Equals(IMAGE)){
			guiContentDict.Add (index.ToString(), new GUIContent (imagePath.texture));
		}
		//    textureDict.Add (imagePath.url, imagePath.texture);
		
		
		imagePath.Dispose();
		imagePath = null;
		
		
	}
	public void TestArray(){
		tex = new Texture2D(100, 100);
		// A small 64x64 Unity logo encoded into a PNG.
		byte[] pngBytes = new byte[] {
			0x89,0x50,0x4E,0x47,0x0D,0x0A,0x1A,0x0A,0x00,0x00,0x00,0x0D,0x49,0x48,0x44,0x52,
			0x00,0x00,0x00,0x40,0x00,0x00,0x00,0x40,0x08,0x00,0x00,0x00,0x00,0x8F,0x02,0x2E,
			0x02,0x00,0x00,0x01,0x57,0x49,0x44,0x41,0x54,0x78,0x01,0xA5,0x57,0xD1,0xAD,0xC4,
			0x30,0x08,0x83,0x81,0x32,0x4A,0x66,0xC9,0x36,0x99,0x85,0x45,0xBC,0x4E,0x74,0xBD,
			0x8F,0x9E,0x5B,0xD4,0xE8,0xF1,0x6A,0x7F,0xDD,0x29,0xB2,0x55,0x0C,0x24,0x60,0xEB,
			0x0D,0x30,0xE7,0xF9,0xF3,0x85,0x40,0x74,0x3F,0xF0,0x52,0x00,0xC3,0x0F,0xBC,0x14,
			0xC0,0xF4,0x0B,0xF0,0x3F,0x01,0x44,0xF3,0x3B,0x3A,0x05,0x8A,0x41,0x67,0x14,0x05,
			0x18,0x74,0x06,0x4A,0x02,0xBE,0x47,0x54,0x04,0x86,0xEF,0xD1,0x0A,0x02,0xF0,0x84,
			0xD9,0x9D,0x28,0x08,0xDC,0x9C,0x1F,0x48,0x21,0xE1,0x4F,0x01,0xDC,0xC9,0x07,0xC2,
			0x2F,0x98,0x49,0x60,0xE7,0x60,0xC7,0xCE,0xD3,0x9D,0x00,0x22,0x02,0x07,0xFA,0x41,
			0x8E,0x27,0x4F,0x31,0x37,0x02,0xF9,0xC3,0xF1,0x7C,0xD2,0x16,0x2E,0xE7,0xB6,0xE5,
			0xB7,0x9D,0xA7,0xBF,0x50,0x06,0x05,0x4A,0x7C,0xD0,0x3B,0x4A,0x2D,0x2B,0xF3,0x97,
			0x93,0x35,0x77,0x02,0xB8,0x3A,0x9C,0x30,0x2F,0x81,0x83,0xD5,0x6C,0x55,0xFE,0xBA,
			0x7D,0x19,0x5B,0xDA,0xAA,0xFC,0xCE,0x0F,0xE0,0xBF,0x53,0xA0,0xC0,0x07,0x8D,0xFF,
			0x82,0x89,0xB4,0x1A,0x7F,0xE5,0xA3,0x5F,0x46,0xAC,0xC6,0x0F,0xBA,0x96,0x1C,0xB1,
			0x12,0x7F,0xE5,0x33,0x26,0xD2,0x4A,0xFC,0x41,0x07,0xB3,0x09,0x56,0xE1,0xE3,0xA1,
			0xB8,0xCE,0x3C,0x5A,0x81,0xBF,0xDA,0x43,0x73,0x75,0xA6,0x71,0xDB,0x7F,0x0F,0x29,
			0x24,0x82,0x95,0x08,0xAF,0x21,0xC9,0x9E,0xBD,0x50,0xE6,0x47,0x12,0x38,0xEF,0x03,
			0x78,0x11,0x2B,0x61,0xB4,0xA5,0x0B,0xE8,0x21,0xE8,0x26,0xEA,0x69,0xAC,0x17,0x12,
			0x0F,0x73,0x21,0x29,0xA5,0x2C,0x37,0x93,0xDE,0xCE,0xFA,0x85,0xA2,0x5F,0x69,0xFA,
			0xA5,0xAA,0x5F,0xEB,0xFA,0xC3,0xA2,0x3F,0x6D,0xFA,0xE3,0xAA,0x3F,0xEF,0xFA,0x80,
			0xA1,0x8F,0x38,0x04,0xE2,0x8B,0xD7,0x43,0x96,0x3E,0xE6,0xE9,0x83,0x26,0xE1,0xC2,
			0xA8,0x2B,0x0C,0xDB,0xC2,0xB8,0x2F,0x2C,0x1C,0xC2,0xCA,0x23,0x2D,0x5D,0xFA,0xDA,
			0xA7,0x2F,0x9E,0xFA,0xEA,0xAB,0x2F,0xDF,0xF2,0xFA,0xFF,0x01,0x1A,0x18,0x53,0x83,
			0xC1,0x4E,0x14,0x1B,0x00,0x00,0x00,0x00,0x49,0x45,0x4E,0x44,0xAE,0x42,0x60,0x82,
		};
		// Load data into the texture.
		tex.LoadImage(pngBytes);
		int val = 0;
		string key;
		for (int x = 0; x < 15; x+=3) {
		
				
				guiContentDict.Add ("img" + x.ToString(),new GUIContent(tex));

				guiContentDict.Add ("des" + (x+1).ToString(),new GUIContent("description" + x.ToString() ));

			guiContentDict.Add ("but" + (x+2).ToString(),new GUIContent("button" + x.ToString() ));
		
		}
		guiArray = new GUIContent[guiContentDict.Count];
		guiContentDict.Values.CopyTo (guiArray, 0);
		
	}

	void OnGUI() {
		if (!DrawTable)
			return;
		int top = 36;
		int dim = GridRowHeight;
		
		//GUI.skin.button.fontSize = 36;
		GUI.skin.button.normal.textColor = Color.white;
		GUI.skin.button.active.textColor = Color.black;
		GUI.skin.button.focused.textColor = Color.white;
		GUI.skin.button.normal.background = buttonTex;
		GUI.skin.button.active.background = buttonTex;
		GUI.skin.button.focused.background = buttonTex;
		GUI.skin.button.hover.background = buttonTex;
		
		
		
		//GUI.skin.button.normal.background = buttonTex;
		
		scrollPosition = GUILayout.BeginScrollView(scrollPosition, GUILayout.Width(actualWidth), GUILayout.Height(actualHeight)); 
		//GUI.backgroundColor = Color.clear;
		GUILayout.BeginVertical ();
		int rowIndex= 0;
		
		int val;
		int offset = 0;
		string sKey = "";
		bool bContinue = false;
		int x = 0;
		//for(int x = 0; x<guiContentDict.Count;x+=cols ) {
		foreach (KeyValuePair<string, GUIContent> content in guiContentDict) {
			
			//get key first

			sKey = content.Key.ToString ();
			Debug.Log (sKey);
			if (sKey.StartsWith ("img")) {
				image = content.Value;
				bContinue = false;
			} else if (sKey.StartsWith ("des")) {
				Description = content.Value;
				bContinue = false;
			} else if (sKey.StartsWith ("but")) {
				ButtonName = content.Value;
				bContinue = true;
				x++;
			}
		}
			
			/*if(!guiContentDict.TryGetValue(x.ToString(),out image)){
				Debug.Log ("key " + x.ToString() + " not found");
			}else{
				Debug.Log ("key " + x.ToString() + " found");
			} 
			
			
			
			if(!guiContentDict.TryGetValue ((x+1).ToString(),out Description)){
				Debug.Log ("key " + (x+1).ToString() + " not found");
			}else{
				Debug.Log ("key " + (x+1).ToString() + " found");
			}
			
			
			
			
			if(!guiContentDict.TryGetValue ((x+2).ToString(),out ButtonName)){
				Debug.Log ("key " + (x+2).ToString() + " not found");
			}else{
				Debug.Log ("key " + (x+2).ToString() + " found");
			} */
		/*		if(bContinue){
			GUI.skin.box.border = new RectOffset(0,0,0,0);
			//	GUI.skin.box.normal.background  = whiteTexture;
			GUILayout.Box("",GUILayout.Height(1.0f));
			//GUI.DrawTexture (new Rect (0, ((dim * (x+1)-2)), actualWidth, 2), lineTex);
			GUILayout.BeginHorizontal ();
			//textFont
			//GUI.DrawTexture(new Rect(0,(dim*x) + 2,dim,dim),tex);
			GUILayout.Label (image,GUILayout.Height(ActualImageWidth),GUILayout.Width(ActualImageWidth));
			GUI.skin.label.fontStyle = FontStyle.Normal;
			
			GUI.skin.label.font = textFont;
			
			GUILayout.Label (Description,GUILayout.Width(ActualDescriptionWidth),GUILayout.Height(ActualImageWidth));
			
			GUI.skin.button.fontStyle = FontStyle.Bold;
			
			GUI.skin.button.font = buttonFont;
			if(GUILayout.Button (ButtonName,GUILayout.Height(ActualImageWidth))){
				RowIndex = x ;
				Debug.Log ("Row :" + RowIndex.ToString());
			}
			GUILayout.EndHorizontal ();
				}
			
			//	GUILayout.EndVertical ();
			//		GUILayout.BeginVertical ();
			//	GUILayout.BeginHorizontal ();
			//	GUI.DrawTexture (new Rect (0, ((dim * (x+1)-2)), actualWidth, 2), lineTex);
			//	GUILayout.EndHorizontal ();
			
		}
		
		
		GUILayout.EndVertical ();
		
		
		
		
		GUILayout.EndScrollView(); */

	/*	if (!DrawTable)
			return;
		//GUI.SelectionGrid(Rect position, int selected, GUIContent[] content, int xCount);
		
		//GUI.contentColor = Color.black; changes the color of text of picture
		//GUI.contentColor = Color.yellow;
	//	GUI.backgroundColor = Color.yellow;
		//GUI.color = Color.yellow;
		//GUI.contentColor = Color.yellow;

	//	GUI.backgroundColor = Color.clear;
	//	mCellStyle = new GUIStyle ();
	//	mCellStyle.normal.background = whiteTexture;

	//	GUI.color = Color.white;
	//	GUI.contentColor = Color.white;
	//	GUI.skin.button.normal.background = whiteTexture;
	//	GUI.skin.button.active.background = 
		int top = 36;
		int dim = GridRowHeight;

		//GUI.skin.button.fontSize = 36;
		GUI.skin.button.normal.textColor = Color.white;
		GUI.skin.button.active.textColor = Color.black;
		GUI.skin.button.focused.textColor = Color.white;
		GUI.skin.button.normal.background = buttonTex;
		GUI.skin.button.active.background = buttonTex;
		GUI.skin.button.focused.background = buttonTex;
		GUI.skin.button.hover.background = buttonTex;



		//GUI.skin.button.normal.background = buttonTex;

		scrollPosition = GUILayout.BeginScrollView(scrollPosition, GUILayout.Width(actualWidth), GUILayout.Height(actualHeight)); 
		//GUI.backgroundColor = Color.clear;
		GUILayout.BeginVertical ();
		int rowIndex= 0;
		GUIContent Description = null;
		GUIContent ButtonName = null;
		GUIContent image = null;
		int val;
		int offset = 0;
		for(int x = 0; x<guiContentDict.Count;x+=cols ) {
			if(!guiContentDict.TryGetValue(x.ToString(),out image)){
				Debug.Log ("key " + x.ToString() + " not found");
			}else{
				Debug.Log ("key " + x.ToString() + " found");
			} 
			
			
			
			if(!guiContentDict.TryGetValue ((x+1).ToString(),out Description)){
				Debug.Log ("key " + (x+1).ToString() + " not found");
			}else{
				Debug.Log ("key " + (x+1).ToString() + " found");
			}
			
			
			
			
			if(!guiContentDict.TryGetValue ((x+2).ToString(),out ButtonName)){
				Debug.Log ("key " + (x+2).ToString() + " not found");
			}else{
				Debug.Log ("key " + (x+2).ToString() + " found");
			}
			GUI.skin.box.border = new RectOffset(0,0,0,0);
		//	GUI.skin.box.normal.background  = whiteTexture;
			GUILayout.Box("",GUILayout.Height(1.0f));
			//GUI.DrawTexture (new Rect (0, ((dim * (x+1)-2)), actualWidth, 2), lineTex);
			GUILayout.BeginHorizontal ();
			//textFont
			//GUI.DrawTexture(new Rect(0,(dim*x) + 2,dim,dim),tex);
			GUILayout.Label (image,GUILayout.Height(ActualImageWidth),GUILayout.Width(ActualImageWidth));
			GUI.skin.label.fontStyle = FontStyle.Normal;
			
			GUI.skin.label.font = textFont;



				
				

			GUILayout.Label (Description,GUILayout.Width(ActualDescriptionWidth),GUILayout.Height(ActualImageWidth));

			GUI.skin.button.fontStyle = FontStyle.Bold;
			
			GUI.skin.button.font = buttonFont;
			if(GUILayout.Button (ButtonName,GUILayout.Height(ActualImageWidth))){
				RowIndex = x /cols;
				Debug.Log ("Row :" + RowIndex.ToString());
			}
			GUILayout.EndHorizontal ();

		
			//	GUILayout.EndVertical ();
//		GUILayout.BeginVertical ();
		//	GUILayout.BeginHorizontal ();
		//	GUI.DrawTexture (new Rect (0, ((dim * (x+1)-2)), actualWidth, 2), lineTex);
		//	GUILayout.EndHorizontal ();
		}


		GUILayout.EndVertical ();

	


		GUILayout.EndScrollView();

		*/
		
	}
	
}


