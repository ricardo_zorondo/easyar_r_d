﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;
//using TheNextFlow.UnityPlugins;

public class AppearitionLocationAR : MonoBehaviour
{
	private bool gyroBool;
	private Gyroscope gyro;
	private Quaternion rotFix;
	GameObject camGrandparent;
	public const float EARTH_RADIUS = 6372797.560856f; // radius of earth in meters
	public GameObject poi;
	public GameObject multiplePOI;
	public Texture poiImage;
	public string CMSAuthorizationToken="";
	public string CMSEndPoint="";
	public static LocationCollection allLocations;
	public Texture textbackground;
	Location[] locations;
	public GameObject POIDetailsScreen;
	public Sprite defaultPOIInfoImage;
	bool cancellable=false;
	LatLong currentUserLocation;
	public float viewSliceAnglePercent=10f;
	ArrayList allOverlappingLocations;
	ArrayList locationsArray;
	public float overlapAngle=18;

	float screenSize;

	GameObject[] pois;

	public float countdown = 1.0f;
	Camera loccamera;
	public enum LocationState
	{
		Disabled,
		TimedOut,
		Failed,
		Enabled


	}

	private LocationState state;


	public void Awake ()
	{

		Transform currentParent = transform.parent;
		GameObject camParent = new GameObject ("GyroCamParent");
		camParent.transform.position = transform.position;
		transform.parent = camParent.transform;
		camGrandparent = new GameObject ("GyroCamGrandParent");
		camGrandparent.transform.position = transform.position;
		camParent.transform.parent = camGrandparent.transform;
		camGrandparent.transform.parent = currentParent;

		//Enable compass
		Input.compass.enabled = true;

		gyroBool = SystemInfo.supportsGyroscope;


		
		if (gyroBool) {
			
			gyro = Input.gyro;
			gyro.enabled = true;
			
			if (Screen.orientation == ScreenOrientation.LandscapeLeft) {
				camParent.transform.eulerAngles = new Vector3 (90, 180, 0);
			} else if (Screen.orientation == ScreenOrientation.Portrait) {
				camParent.transform.eulerAngles = new Vector3 (90, 180, 0);
			} else if (Screen.orientation == ScreenOrientation.PortraitUpsideDown) {
				camParent.transform.eulerAngles = new Vector3 (90, 180, 0);
			} else if (Screen.orientation == ScreenOrientation.LandscapeRight) {
				camParent.transform.eulerAngles = new Vector3 (90, 180, 0);
			} else {
				camParent.transform.eulerAngles = new Vector3 (90, 180, 0);
			}
			
			if (Screen.orientation == ScreenOrientation.LandscapeLeft) {
				rotFix = new Quaternion (0, 0, 1, 0);
			} else if (Screen.orientation == ScreenOrientation.Portrait) {
				rotFix = new Quaternion (0, 0, 1, 0);
			} else if (Screen.orientation == ScreenOrientation.PortraitUpsideDown) {
				rotFix = new Quaternion (0, 0, 1, 0);
			} else if (Screen.orientation == ScreenOrientation.LandscapeRight) {
				rotFix = new Quaternion (0, 0, 1, 0);
			} else {
				rotFix = new Quaternion (0, 0, 1, 0);
			}
			

		} else {
			Debug.Log("AppearitionLocationAR: No Gyro");
			#if !UNITY_EDITOR
			NativeDialogs.Instance.HideProgressDialog();
			#endif
			#if UNITY_ANDROID
			NativeDialogs.Instance.ShowMessageBox("Error","Locations is not supported on this device.",new String[]{"Ok"},cancellable, (string button) => {});
			#endif


		}
	}




	IEnumerator Start()
	{
		// Start service before querying location
		loccamera = GetComponent<Camera> ();
		#if !UNITY_EDITOR
		NativeDialogs.Instance.ShowProgressDialog("Loading Locations", "Please wait while the locations are loading...", cancellable, false);
		#endif

		state = LocationState.Disabled;
		// First, check if user has location service enabled
		if (!Input.location.isEnabledByUser) {
			//show alert that GPS needs to be enabled for this for android in iOS its handled on its own...
			Debug.Log("AppearitionLocationAR: No locations enabled");
			#if !UNITY_EDITOR
			NativeDialogs.Instance.HideProgressDialog();
			#endif
			#if UNITY_ANDROID
			NativeDialogs.Instance.ShowMessageBox("Turn On GPS","Please turn on the GPS from the settings",new String[]{"Ok"},cancellable, (string button) => {Application.Quit();});
			#endif
			//StartCoroutine(InitialiseAppearition());
			yield break;

		}
		// 10 m distance 
		Input.location.Start(10f,10f);

		
		// Wait until service initializes
		int maxWait = 20;
		while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
		{
			yield return new WaitForSeconds(1);
			Debug.Log("AppearitionLocationAR:maxWait"+maxWait);
			maxWait--;
		}
		
		// Service didn't initialize in 20 seconds
		if (maxWait < 1)
		{
			print("Location:Timed out");
			#if !UNITY_EDITOR
			NativeDialogs.Instance.HideProgressDialog();
			#endif
			#if UNITY_ANDROID
			NativeDialogs.Instance.ShowMessageBox("Error","Unable to activate GPS",new String[]{"Ok"},cancellable, (string button) => {});
			#endif
			state = LocationState.TimedOut;
			yield break;
		}
		
		// Connection has failed
		if (Input.location.status == LocationServiceStatus.Failed)
		{
			Debug.Log("AppearitionLocationAR: Location Service failed");
			print("Location:Unable to determine device location");
			#if UNITY_ANDROID
			NativeDialogs.Instance.ShowMessageBox("Error","Unable to activate GPS",new String[]{"Ok"},cancellable, (string button) => {});
			#endif
			#if !UNITY_EDITOR
			NativeDialogs.Instance.HideProgressDialog();
			#endif
			state = LocationState.Failed;
			yield break;
		}
		else
		{
			state = LocationState.Enabled;
			Debug.Log("AppearitionLocationAR:New Start of Location");
			#if UNITY_IOS
			yield return new WaitForSeconds(3f);
			#endif

			StartCoroutine(InitialiseAppearition());



		}
		


	}



	IEnumerator InitialiseAppearition() {
	
		WWW actualRequest = GetCMSLoader(CMSEndPoint);
		yield return actualRequest;
		if(actualRequest.error == null) {
			Debug.Log(actualRequest.text);
			allLocations= new LocationCollection(actualRequest.text);
			yield return StartCoroutine(HandleLocationRendering());

		} else {
			Debug.LogWarning(actualRequest.error);
			#if !UNITY_EDITOR
			NativeDialogs.Instance.HideProgressDialog();
			#endif
		}
	}




	private IEnumerator HandleLocationRendering(){
		currentUserLocation = new LatLong(Input.location.lastData.latitude,Input.location.lastData.longitude);
		locations = allLocations.getUnsortedLocations ();

		//get all locations as given by CM calculate the bearing for all locations and sort by bearing
		for (int i=0; i<locations.Length; i++) {
			//this should always be from the current position to destination.
			locations[i].bearingAngle= BearingBetweenCoordinates(currentUserLocation.Lat,currentUserLocation.Long,locations[i].latitude,locations[i].longitude);
			locations[i].distanceInMeters=DistanceInMetresFrom(currentUserLocation,new LatLong(locations[i].latitude,locations[i].longitude));
			locations[i].positioninWorld=calculatePOIPosition (locations[i].distanceInMeters,locations[i].bearingAngle);
		}


		locations = allLocations.getSortedLocations ();




		FindOverLappings ();


		for (int i=0; i<allOverlappingLocations.Count; i++) {

			ArrayList locArray = (ArrayList)allOverlappingLocations[i];

			if(locArray.Count>1){
				//overlapping pois
				locArray.Sort(new SortLocationsAssending());
				Location nearestLocationWithLabel=null;
				nearestLocationWithLabel=(Location)locArray[0];
				Debug.Log("nearestLocationWithLabel::"+nearestLocationWithLabel.nameShort);
				if(nearestLocationWithLabel!=null){
					GameObject poiclone = Instantiate (multiplePOI, nearestLocationWithLabel.positioninWorld, Quaternion.Euler(new Vector3(0f,nearestLocationWithLabel.bearingAngle,0f))) as GameObject;
					poiclone.name=nearestLocationWithLabel.id.ToString();
					GameObject poiDetails = poiclone.transform.FindChild("POIDetails").gameObject;
					GameObject poiTitle = poiDetails.transform.FindChild("Title").gameObject;
					poiTitle.GetComponent<TextMesh>().text=nearestLocationWithLabel.nameShort;	
					poiDetails.transform.FindChild("Distance").GetComponent<TextMesh>().text= (nearestLocationWithLabel.distanceInMeters/1000).ToString("0.00")+" KM";	
					poiDetails.transform.FindChild("BackgroundText").GetComponent<Renderer>().material.mainTexture=textbackground;
					poiDetails.GetComponent<POIInformation>().id=nearestLocationWithLabel.id;
					poiDetails.GetComponent<POIInformation>().latitude=nearestLocationWithLabel.latitude;
					poiDetails.GetComponent<POIInformation>().longitude=nearestLocationWithLabel.longitude;
					poiDetails.GetComponent<POIInformation>().nameShort=nearestLocationWithLabel.nameShort;
					poiDetails.GetComponent<POIInformation>().arrayIndex=i;
					if(nearestLocationWithLabel.imageUrl!=null && nearestLocationWithLabel.imageUrl.Length>0 && !"null".Equals(nearestLocationWithLabel.imageUrl)){
						StartCoroutine(ShowLabelImages(poiDetails,nearestLocationWithLabel.imageUrl));
					}
					else{
						
						poiDetails.transform.FindChild("POIImage").GetComponent<Renderer>().material.mainTexture=poiImage;
					}

				}




			}else{
				//single poi

				Location singleLocation = (Location)locArray[0];
				if(singleLocation.displayLabel){
					GameObject poiclone = Instantiate (poi, singleLocation.positioninWorld, Quaternion.Euler(new Vector3(0f,singleLocation.bearingAngle,0f))) as GameObject;
					poiclone.name=i.ToString();
					GameObject poiDetails = poiclone.transform.FindChild("POIDetails").gameObject;
					GameObject poiTitle = poiDetails.transform.FindChild("Title").gameObject;
					poiTitle.GetComponent<TextMesh>().text=singleLocation.nameShort;
					
					poiDetails.transform.FindChild("Distance").GetComponent<TextMesh>().text= (singleLocation.distanceInMeters/1000).ToString("0.00")+" KM";	
					poiDetails.transform.FindChild("BackgroundText").GetComponent<Renderer>().material.mainTexture=textbackground;
					poiDetails.GetComponent<POIInformation>().id=singleLocation.id;
					poiDetails.GetComponent<POIInformation>().latitude=singleLocation.latitude;
					poiDetails.GetComponent<POIInformation>().longitude=singleLocation.longitude;
					poiDetails.GetComponent<POIInformation>().nameShort=singleLocation.nameShort;
					poiDetails.GetComponent<POIInformation>().arrayIndex=i;
					if(singleLocation.imageUrl!=null && singleLocation.imageUrl.Length>0 && !"null".Equals(singleLocation.imageUrl)){
						StartCoroutine(ShowLabelImages(poiDetails,singleLocation.imageUrl));
					}
					else{
						
						poiDetails.transform.FindChild("POIImage").GetComponent<Renderer>().material.mainTexture=poiImage;
					}
				}

			}


		}



		pois = GameObject.FindGameObjectsWithTag("poi_tag");

		// Stop service if there is no need to query location updates continuously

		#if UNITY_IOS
		if(locations.Length>0){
		CalibrateCamera (locations[0].trueNorthReading);
		}
		#endif


		//Input.location.Stop ();
		yield return null;
	#if !UNITY_EDITOR
		NativeDialogs.Instance.HideProgressDialog();
	#endif		
		
	}



	private class SortLocationsAssending: IComparer
	{
		int IComparer.Compare(object a, object b)
		{
			Location c1=(Location)a;
			Location c2=(Location)b;
			
			if (c1.distanceInMeters < c2.distanceInMeters)
				return -1;
			
			if (c1.distanceInMeters > c2.distanceInMeters)
				return 1;
			
			else
				return 0;
		}
	}


	private void FindOverLappings(){
		if (allOverlappingLocations != null) {
			allOverlappingLocations.Clear();
			allOverlappingLocations=null;

		}
		if (locationsArray != null) {
			locationsArray.Clear();
			locationsArray=null;
			
		}
		allOverlappingLocations= new ArrayList();
		locationsArray = new ArrayList ();

		if (locations.Length > 0) {
			locationsArray.Add(locations[0]);


			if(locations.Length==1){
				allOverlappingLocations.Add(locationsArray);
				return;
			}
			else{

				//as you have already added location at index 0.
				Location currentPOI=locations[0];
				Location previousPOI=locations[0];


				//overlapLocation= new ArrayList();

				for(int i=1;i<locations.Length;i++){

					previousPOI=currentPOI;
					currentPOI=locations[i];


					float differenceBearing = previousPOI.bearingAngle-currentPOI.bearingAngle;

					if(differenceBearing<overlapAngle){
						//overlapping POI'S
						locationsArray.Add(currentPOI);
						continue;
					}else{
						allOverlappingLocations.Add(locationsArray);
						//Create and add to separate the list
						locationsArray= new ArrayList();
						locationsArray.Add(currentPOI);
					}

				}

				//Add the last array
				allOverlappingLocations.Add(locationsArray);



			}

		} else {

			return;
		}


	}


	private void UpdatePOIVisibility(Vector3 point, GameObject poi){
		screenSize = Screen.width;
		Vector3 screenPoint = loccamera.WorldToScreenPoint(point);
		if (screenPoint.x >= screenSize / 2f - (screenSize * viewSliceAnglePercent/100f) && screenPoint.x <= screenSize / 2f + (screenSize * viewSliceAnglePercent/100f)) {

			poi.SetActive (true);
		} else {
			poi.SetActive (false);
		}
	}


	private void CalibrateCamera(float trueHeadingValue){

		Vector3 newPosition = new Vector3(camGrandparent.transform.localEulerAngles.x,trueHeadingValue,camGrandparent.transform.localEulerAngles.z);
		camGrandparent.transform.localEulerAngles = newPosition;


	}
	//small label
	private IEnumerator ShowLabelImages(GameObject poiDetails, string imageUrl){
		GameObject imageOnTarget=  poiDetails.transform.FindChild("POIImage").gameObject;
		imageOnTarget.GetComponent<Renderer>().material.mainTexture = new Texture2D(4, 4, TextureFormat.RGBA32, false);
		WWW imagePath = new WWW(imageUrl);
		yield return imagePath;



		imagePath.LoadImageIntoTexture(imageOnTarget.GetComponent<Renderer>().material.mainTexture as Texture2D);
		imagePath.Dispose();
		imagePath = null;
		
		
	}
	
	private WWW GetCMSLoader(string url) {
		Dictionary<string, string> headers = new Dictionary<string, string>();
		headers.Add("authentication-token", CMSAuthorizationToken);
		headers.Add ("Content-Type", "application/json");
		headers.Add("charset", "utf-8");
		WWW actualRequest = new WWW(url, null, headers);
		return actualRequest;
	}

/*	void  Update ()
	{


		if (Input.GetMouseButtonDown (0)) {
			//Debug.Log("Touch ");
			
			//Touch touch = Input.touches[0];
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition); 
			RaycastHit hit = new RaycastHit (); 
			
			
			if (Physics.Raycast (ray, out hit, Mathf.Infinity)) {
				Debug.Log("RayHit::"+hit.transform.gameObject.name);

				int id = hit.transform.gameObject.GetComponentInParent<POIInformation> ().id;
				int arrayindex = hit.transform.gameObject.GetComponentInParent<POIInformation> ().arrayIndex;
				if(((ArrayList)allOverlappingLocations[arrayindex]).Count>1){

					GameObject.Find ("MenuButtonsHandler").GetComponent<MenubuttonsHandler> ().ShowMultiPOIDetails ((ArrayList)allOverlappingLocations[arrayindex]);

				}
				else{

				Location loc = allLocations.SearchByID (id);
				if (loc.displayInfo) {
					
					
					MenubuttonsHandler.selectedLocation = loc;
					GameObject.Find ("MenuButtonsHandler").GetComponent<MenubuttonsHandler> ().ShowPOIDetails ();
					
					}
				}
			}
		}

		if(gyroBool){
			Quaternion quatMap;
			#if UNITY_IOS
			quatMap = gyro.attitude;
			#elif UNITY_ANDROID
			//Debug.Log("Gyro values:"+gyro.attitude.x+"::"+gyro.attitude.y+"::"+gyro.attitude.z+"::"+gyro.attitude.w);
			quatMap = new Quaternion(gyro.attitude.x,gyro.attitude.y,gyro.attitude.z,gyro.attitude.w);
			#endif
			
			transform.localRotation = quatMap * rotFix;
			
		}
		
		countdown -= Time.deltaTime;
		if (countdown <= 0.0f){

		



		


			if (state == LocationState.Enabled) {
				
				float deltaDistance = Haversine(ref currentUserLocation);
				if(deltaDistance>0){
					foreach(GameObject poi in pois){ 

						DestroyImmediate(poi);
					}
					pois=null;
					StartCoroutine(HandleLocationRendering());
					

					
					
					
					
				}

	}
		
			countdown=1f;


		}











	} */


	private IEnumerator ShowInfoImage(GameObject detailImage, string imageUrl){
		WWW imagePath = new WWW(imageUrl);
		yield return imagePath;
		detailImage.GetComponent<Image>().sprite = Sprite.Create(imagePath.texture, new Rect(0, 0, imagePath.texture.width, imagePath.texture.height), new Vector2(0.5f, 0.5f));
		imagePath.Dispose();
		imagePath = null;
		
		
	}





	public static float BearingBetweenCoordinates(float lat1,float lon1,float lat2, float lon2){
		lat1 = lat1 * Mathf.Deg2Rad;
		lat2 = lat2 * Mathf.Deg2Rad;
		float dLon = (float)(lon2 - lon1) * Mathf.Deg2Rad;
		float dPhi = Mathf.Log (Mathf.Tan (lat2 / 2f + Mathf.PI / 4f) / Mathf.Tan (lat1 / 2f + Mathf.PI / 4f));
		if (Mathf.Abs (dLon) > Mathf.PI) {
			dLon= dLon>0 ? -(2f*Mathf.PI-dLon): (2f*Mathf.PI+dLon);
		}
		float bearing = Mathf.Atan2 (dLon, dPhi);
		//bearing = bearing * Mathf.Rad2Deg * -1;
		//initial bearing
		return ((bearing * Mathf.Rad2Deg) + 360) % 360;

		//bearing= ((bearing * Mathf.Rad2Deg) + 360) % 360;


		//Debug.Log ("InitialBearing::" + bearing);
		//Debug.Log ("FinalBearing::" + (bearing+ 180) % 360);

		//final bearing

		//return (bearing+ 180) % 360;
	}
	
	

	
	
	private static float ArcInRadians(LatLong from, LatLong to)
	{
		var latitudeArc = (from.Lat - to.Lat)*Mathf.Deg2Rad;
		var longitudeArc = (from.Long - to.Long)*Mathf.Deg2Rad;
		var latitudeH = Mathf.Sin((float)latitudeArc*0.5f);
		latitudeH *= latitudeH;
		var lontitudeH = Mathf.Sin((float)longitudeArc*0.5f);
		lontitudeH *= lontitudeH;
		var tmp = Mathf.Cos((float)from.Lat*Mathf.Deg2Rad)*Mathf.Cos((float)to.Lat*Mathf.Deg2Rad);
		return 2.0f*Mathf.Asin(Mathf.Sqrt(latitudeH + tmp*lontitudeH));
	}
	
	public static float DistanceInMetresFrom(LatLong to, LatLong from)
	{
		return EARTH_RADIUS * ArcInRadians(from, to);
		
		
	}


	float Haversine(ref LatLong currentUserLocation){

		float newLatitude = Input.location.lastData.latitude;
		float newLongitude = Input.location.lastData.longitude;
		float deltaLatitude = (newLatitude - currentUserLocation.Lat) * Mathf.Deg2Rad;
		float deltaLongitude = (newLongitude - currentUserLocation.Long) * Mathf.Deg2Rad;
		float a = Mathf.Pow (Mathf.Sin (deltaLatitude / 2), 2) + Mathf.Cos (currentUserLocation.Lat * Mathf.Deg2Rad) * Mathf.Cos (newLatitude * Mathf.Deg2Rad) * Mathf.Pow (Mathf.Sin (deltaLongitude / 2), 2);
		currentUserLocation.Lat = newLatitude;
		currentUserLocation.Long = newLongitude;
		float c = 2 * Mathf.Atan2 (Mathf.Sqrt (a), Mathf.Sqrt (1 - a));
		return EARTH_RADIUS * c;



	}
	
	
	//
	public Vector3 calculatePOIPosition(float physicaldistance, float bearingAngle,float height){
		float distance = 25f;
	/*	if (physicaldistance < 1000) {
			distance = 20f;
		} else {
			distance=25f;
		}
*/

		Vector3 pos = Quaternion.AngleAxis(bearingAngle, Vector3.up) * Vector3.forward * distance; 
		pos.y = height;
		return pos;
		
	}


	public Vector3 calculatePOIPosition(float physicaldistance, float bearingAngle){
		float distance = 25f;
		/*if (physicaldistance < 1000) {
			distance = 20f;

		} else {
			distance=25f;
		}
		*/
		Vector3 pos = Quaternion.AngleAxis(bearingAngle, Vector3.up) * Vector3.forward * distance; 

		return pos;
		
	}
	
	





	

}