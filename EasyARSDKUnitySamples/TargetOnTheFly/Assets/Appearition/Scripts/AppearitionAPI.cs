﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;



/*public class ARMediaFile {

	public static string MEDIA_TYPE_3D="3d";
	public static string MEDIA_TYPE_IMAGE="image";
	public static string MEDIA_TYPE_PANAROMA="panaroma";
	public static string MEDIA_TYPE_PDF="pdf";
	public static string MEDIA_TYPE_UNITY_ANDROID="unity_android";
	public static string MEDIA_TYPE_UNITY_IOS="unity_ios";
	public static string MEDIA_TYPE_VIDEO="video";
	public static string MEDIA_TYPE_VIDEO_STREAM="video_stream";
	public static string MEDIA_TYPE_WEBLINK="weblink";
	public static string MEDIA_TYPE_ANIMATION="animation";


	public string animationName, arMediaId, checksum, fileName, language, mediaType, mimeType, url;
	public bool isAutoPlay, isInteractive, isPreDownload, isPrivate, isTracking;
	
	// I'll extract the other things later if it's relevant which it's not now
	//public double resolution
	public ARMediaFile(SimpleJSON.JSONNode node) {
		animationName = node["animationName"];
		arMediaId = node["arMediaId"];
		checksum = node["checksum"];
		fileName = node["fileName"];
		language = node["language"];
		mediaType = node["mediaType"];
		mimeType = node["mimeType"];
		url = node["url"];
	}
}

public class ARImage {
	public string arImageId, checksum, fileName, url;
	
	public ARImage(SimpleJSON.JSONNode node) {
		arImageId = node["arImageId"];
		checksum = node["checksum"];
		fileName = node["fileName"];
		url = node["url"];
	}
}

public class ARExperience {
	public ARImage[] images;
	public ARMediaFile[] assets;
	public int arTargetID;
	public string assetID;
	public bool isPublished;
	public string name;
	public string productID;
	public ARExperience(SimpleJSON.JSONNode node) {
		images = new ARImage[node["arImages"].Count];
		for(int i = 0; i < node["arImages"].Count; ++i) {
			images[i] = new ARImage(node["arImages"][i]);
		}
		assets = new ARMediaFile[node["mediaFiles"].Count];
		for(int i = 0; i < node["mediaFiles"].Count; ++i) {
			assets[i] = new ARMediaFile(node["mediaFiles"][i]);
		}
		arTargetID = node["arTargetId"].AsInt;
		assetID = node["assetId"];
		isPublished = node["isPublished"].AsBool;
		name = node["name"];
		productID = node["productId"];
	}
	public void AddMedia(string toParse) {
		var parsed = SimpleJSON.JSON.Parse(toParse);
		List<ARMediaFile> listed = new List<ARMediaFile>(assets);
		ARMediaFile retV = new ARMediaFile(parsed["Data"]);
		listed.Add(retV);
		assets = listed.ToArray();
		//return retV;
	}
	public void AddImage(string toParse) {
		var parsed = SimpleJSON.JSON.Parse(toParse);
		List<ARImage> listed = new List<ARImage>(images);
		ARImage retV = new ARImage(parsed["Data"]);
		listed.Add(retV);
		images = listed.ToArray();
	}
}

public class ARTargetCollection {
	public ARExperience[] experiences;
	public ARTargetCollection(string toParse) {
		var parsed = SimpleJSON.JSON.Parse(toParse);
		experiences = new ARExperience[parsed["Data"].Count];
		for(int i = 0; i < parsed["Data"].Count; ++i) {
			experiences[i] = new ARExperience(parsed["Data"][i]);
		}
	}
	public ARExperience AddNewExperience(string toParse) {
		var parsed = SimpleJSON.JSON.Parse(toParse);
		List<ARExperience> listed = new List<ARExperience>(experiences);
		ARExperience retV = new ARExperience(parsed["Data"]);
		listed.Add(retV);
		experiences = listed.ToArray();
		return retV;
	}
}
*/



public class AssetCollection {
	public AssetCollection(string toParse) {
		var parsed = SimpleJSON.JSON.Parse(toParse);
		success = parsed["IsSuccess"].AsBool;
		if(!success) {
			Debug.LogError("Something went wrong!");
			return;
		}
		assets = new Asset[parsed["Data"]["assets"].Count];
		for(int i = 0; i < parsed["Data"]["assets"].Count; ++i) {
			assets[i] = new Asset(parsed["Data"]["assets"][i]);

		}
		errors = new string[parsed["Errors"].Count];
		for(int i = 0; i < parsed["Errors"].Count; ++i) {
			errors[i] = parsed["Errors"][i];
		}
	}
	public bool success;
	public Asset[] assets;
	public string[] errors;
	public override string ToString ()
	{
		System.Text.StringBuilder build = new System.Text.StringBuilder();
		foreach(Asset asset in assets) {
			build.Append(asset.ToString());
			build.Append("\n");
		}
		return build.ToString();
	}

	public Asset[] getAssets(){

		return assets;
	}


	/*	public Asset SearchByName(string name) {
		string platform = "";
		if(Application.platform == RuntimePlatform.Android) {
			platform = "Android";
		}
		if(Application.platform == RuntimePlatform.IPhonePlayer) {
			platform = "iOS";
		}
		if(Application.platform == RuntimePlatform.WindowsEditor) {
			platform = "iOS";
		}
		foreach(Asset asset in assets) {
			if (asset.mediaName == name && asset.animationName == platform) {
				return asset;
			}
		}
		return null;
	}
*/
	public Asset SearchForMediaName(string name) {

		foreach(Asset asset in assets) {
			foreach(MediaFile media in asset.mediaFiles){
			if (media.fileName == name) {
				return asset;
			}
		}
	}
		return null;
	}

	public Asset SearchForTargetName(string name) {
		
		foreach(Asset asset in assets) {
			foreach(targetImage targetImage in asset.targetImages){
				if (targetImage.fileName == name) {
					return asset;
				}
			}
		}
		return null;
	}

	public Asset SearchByID(string id) {


		foreach(Asset asset in assets) {

			if (asset.assetId == id) {
				return asset;
			}
		}
		return null;
	}
}
public class Asset {
	public Asset(SimpleJSON.JSONNode parseNode) {
		assetId = parseNode["assetId"];
		//Debug.Log(assetId);
		productId = parseNode ["productId"].AsInt;
		mediaFiles = new MediaFile[parseNode ["mediaFiles"].Count];
	//	Debug.Log ("MediaFiles are " + parseNode ["mediaFiles"].Count);
		for (int i=0; i< parseNode["mediaFiles"].Count; ++i) {
			mediaFiles[i]= new MediaFile(parseNode["mediaFiles"][i]);

		}
	//	Debug.Log ("targetImages are " + parseNode ["targetImages"].Count);
		targetImages = new targetImage[parseNode ["targetImages"].Count];
		for (int i=0; i< parseNode["targetImages"].Count; ++i) {
			targetImages[i]= new targetImage(parseNode["targetImages"][i]);
			
		}
	}
	public string assetId, name ;
	public int productId;
	public MediaFile[] mediaFiles;
	public targetImage[] targetImages;


	public override string ToString ()
	{
		return "id: " + assetId + ", " + "name: " + name;
	}
}

public class targetImage{
	public string arImageId,fileName,mediaUrl;
	public targetImage(SimpleJSON.JSONNode parseNode){
//		Debug.Log ("targetImages parseNode is: " + parseNode.ToString ());
		arImageId = parseNode["arImageId"];

		fileName = parseNode["fileName"];

		mediaUrl = parseNode["url"];
	}
}


public class MediaFile{

	public string mediaType, mediaUrl, checksum, requiresAuthToken, animationName,fileName,language,lastModified,mimeType;
	public bool isAutoPlay,isInteractive, isPreDownload,isPrivate ,isTracking;
	public Dimension scale, translation, rotation;

	public MediaFile(SimpleJSON.JSONNode parseNode){
		//Debug.Log ("MediaFile parseNode is: " + parseNode.ToString ());
		animationName = parseNode["animationName"];
		checksum = parseNode["checksum"];
		fileName = parseNode["fileName"];
		isAutoPlay = parseNode["isAutoPlay"].AsBool;
		isInteractive = parseNode["isInteractive"].AsBool;
		isPreDownload = parseNode["isPreDownload"].AsBool;
		isPrivate = parseNode["isPrivate"].AsBool;
		isTracking = parseNode["isTracking"].AsBool;
		language = parseNode["language"];
		lastModified = parseNode["lastModified"];
		mediaType = parseNode["mediaType"];
		mimeType = parseNode["mimeType"];
		scale = new Dimension(parseNode["scaleX"].AsFloat,parseNode["scaleY"].AsFloat,parseNode["scaleZ"].AsFloat);
		translation = new Dimension(parseNode["translationX"].AsFloat,parseNode["translationY"].AsFloat,parseNode["translationZ"].AsFloat);
		rotation = new Dimension(parseNode["rotationX"].AsFloat,parseNode["rotationY"].AsFloat,parseNode["rotationZ"].AsFloat);
		mediaUrl = parseNode["url"];



	}




}



public class Dimension {
	public Dimension(SimpleJSON.JSONNode parseNode) {
		x = parseNode["x"].AsFloat;
		y = parseNode["y"].AsFloat;
		z = parseNode["z"].AsFloat;
	}


	public Dimension(float tx, float ty , float tz){
		x = tx;
		y = ty;
		z = tz;

	}

	public override string ToString ()
	{
		return "X: " + x + ", y: "+y + ",z: " + z;
	}
	public float x,y,z;
}



public class LocationCollection{
	public bool success;
	public Location[] locations;
	//public ArrayList locationsArray;
	public string[] errors;
	
	public LocationCollection(string toParse) {
		var parsed = SimpleJSON.JSON.Parse(toParse);
		//Debug.Log("Parsed");
		success = parsed["IsSuccess"].AsBool;
		
		if(!success) {
			Debug.LogError("Something went wrong!");
			return;
		}

		LocationInfo userLocation = Input.location.lastData;
		float trueNorth = Input.compass.trueHeading;
		locations = new Location[parsed["Data"]["locations"].Count];


		//locations = new ArrayList ();
		for(int i = 0; i < parsed["Data"]["locations"].Count; ++i) {
			locations[i] = new Location(parsed["Data"]["locations"][i],userLocation,trueNorth);
			//locations.Add(new Location(parsed["Data"]["locations"][i]));
			
		}
		errors = new string[parsed["Errors"].Count];
		for(int i = 0; i < parsed["Errors"].Count; ++i) {
			errors[i] = parsed["Errors"][i];
		}
	}
	
	//public Location[] getLocations(){
	public Location[] getSortedLocations(){
		//locations.Sort ();
		Array.Sort (locations, Location.sortYearDescending());
		return locations;
	}


	public Location[] getUnsortedLocations(){
		//locations.Sort ();
		//Array.Sort (locations, Location.sortYearDescending());
		return locations;
	}
	
	public Location SearchByID(int id) {
		foreach(Location location in locations) {
			if (location.id == id) {
				return location;
			}
		}
		return null;
	}
	
}




public class Location{
	public const float EARTH_RADIUS = 6372797.560856f; // radius of earth in meters
	public string  markerUrl,markerChecksum, imageUrl,imageChecksum, infoImageUrl,infoImageChecksum, name, nameShort, infoHtml, address;
	public bool displayLabel, displayInfo;
	public int id;
	public float latitude, longitude, altitude,bearingAngle,trueNorthReading;
	public float distanceInMeters;
	public Vector3 positioninWorld;
	public Sprite labelImage;
	
	
	
	public override string ToString ()
	{
		return "id: " + id + ", " + "name: " + name;
	}
	public Location(SimpleJSON.JSONNode parseNode, LocationInfo userLocation, float tureHeading) {
		
		id = parseNode["id"].AsInt;
		markerUrl = parseNode ["markerUrl"];
		markerChecksum = parseNode ["markerChecksum"];
		imageUrl = parseNode ["imageUrl"];
		imageChecksum = parseNode ["imageChecksum"];
		infoImageUrl = parseNode ["infoImageUrl"];
		infoImageChecksum = parseNode ["infoImageChecksum"];
		name = parseNode ["name"];
		nameShort = parseNode ["nameShort"];
		infoHtml = parseNode ["infoHtml"];
		address = parseNode ["address"];
		latitude = parseNode ["latitude"].AsFloat;
		longitude = parseNode ["longitude"].AsFloat;
		altitude = parseNode ["altitude"].AsFloat;
		displayLabel = parseNode ["displayLabel"].AsBool;
		displayInfo = parseNode ["displayInfo"].AsBool;

		LatLong from = new LatLong();
		from.Lat= userLocation.latitude;
		from.Long=userLocation.longitude;
		
		LatLong to = new LatLong();
		to.Lat = latitude;
		to.Long =longitude;
		//bearingAngle=BearingBetweenCoordinates(Convert.ToSingle(from.Lat),Convert.ToSingle(from.Long),latitude,longitude);
		//distanceInMeters= DistanceInMetresFrom (from, to);
		trueNorthReading = tureHeading;
		

	}

 
	// Nested class to do descending sort on make property.
	private class sortBearingDescendingHelper: IComparer
	{
		int IComparer.Compare(object a, object b)
		{
			Location c1=(Location)a;
			Location c2=(Location)b;
			
			if (c1.bearingAngle < c2.bearingAngle)
				return 1;
			
			if (c1.bearingAngle > c2.bearingAngle)
				return -1;
			
			else
				return 0;
		}
	}


	public static IComparer sortYearDescending()
	{      
		return (IComparer) new sortBearingDescendingHelper();
	}
/*	public static float BearingBetweenCoordinates(float lat1,float lon1,float lat2, float lon2){
		lat1 = lat1 * Mathf.Deg2Rad;
		//lon1 = lon1 * Mathf.Deg2Rad;
		lat2 = lat2 * Mathf.Deg2Rad;
		//lon2 = lon2 * Mathf.Deg2Rad;
		
		
		float dLon = (float)(lon2 - lon1) * Mathf.Deg2Rad;
		
		float dPhi = Mathf.Log (Mathf.Tan (lat2 / 2f + Mathf.PI / 4f) / Mathf.Tan (lat1 / 2f + Mathf.PI / 4f));
		
		if (Mathf.Abs (dLon) > Mathf.PI) {
			dLon= dLon>0 ? -(2f*Mathf.PI-dLon): (2f*Mathf.PI+dLon);
		}
		
		float bearing = Mathf.Atan2 (dLon, dPhi);
		Debug.Log ("Bearing Angle:::"+((bearing * Mathf.Rad2Deg) + 360) % 360);
		
		return ((bearing * Mathf.Rad2Deg) + 360) % 360;
		
		
		
	}


	public static double DistanceInMetresFrom(LatLong to, LatLong from)
	{
		return EARTH_RADIUS * ArcInRadians(from, to);
		
		
	}

	private static double ArcInRadians(LatLong from, LatLong to)
	{
		var latitudeArc = (from.Lat - to.Lat)*Mathf.Deg2Rad;
		var longitudeArc = (from.Long - to.Long)*Mathf.Deg2Rad;
		var latitudeH = Mathf.Sin((float)latitudeArc*0.5f);
		latitudeH *= latitudeH;
		var lontitudeH = Mathf.Sin((float)longitudeArc*0.5f);
		lontitudeH *= lontitudeH;
		var tmp = Mathf.Cos((float)from.Lat*Mathf.Deg2Rad)*Mathf.Cos((float)to.Lat*Mathf.Deg2Rad);
		return 2.0f*Mathf.Asin(Mathf.Sqrt(latitudeH + tmp*lontitudeH));
	}
*/
	
}




