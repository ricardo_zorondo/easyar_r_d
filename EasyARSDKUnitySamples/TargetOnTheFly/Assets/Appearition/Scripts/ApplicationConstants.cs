﻿using UnityEngine;
using System.Collections;

public static class ApplicationConstants {

	public static string MEDIA_TYPE_3D="3d";
	public static string MEDIA_TYPE_IMAGE="image";
	public static string MEDIA_TYPE_PANAROMA="panaroma";
	public static string MEDIA_TYPE_PDF="pdf";
	public static string MEDIA_TYPE_UNITY_ANDROID="unity_android";
	public static string MEDIA_TYPE_UNITY_IOS="unity_ios";
	public static string MEDIA_TYPE_VIDEO="video";
	public static string MEDIA_TYPE_VIDEO_STREAM="video_stream";
	public static string MEDIA_TYPE_WEBLINK="weblink";
	public static string MEDIA_TYPE_ANIMATION="animation";
	public static string MEDIA_TYPE_TRANSPARENT_VIDEO="Transparent Video";



	public static string GA_CATEGORY_SCAN="SCAN";
	public static string GA_CATEGORY_PLAY_MEDIA="PLAY-MEDIA";
	public static string GA_ACTION_FOUND="FOUND";
	public static string GA_ACTION_NOT_FOUND="NOT-FOUND";
	public static string GA_ACTION_NO_MATCHING_ASSET="NO-MATCHING-ASSET";
	public static string GA_ACTION_MATCHING_ASSET_FOUND="MATCHING-ASSET-FOUND";



}
