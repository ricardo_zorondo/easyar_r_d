﻿/**
* Copyright (c) 2015 VisionStar Information Technology (Shanghai) Co., Ltd. All Rights Reserved.
* EasyAR is the registered trademark or trademark of VisionStar Information Technology (Shanghai) Co., Ltd in China
* and other countries for the augmented reality technology developed by VisionStar Information Technology (Shanghai) Co., Ltd.
*/

using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using EasyAR;

namespace EasyARSample
{
    public class ImageTargetManager : MonoBehaviour
    {
        private Dictionary<string, DynamicImageTagetBehaviour> imageTargetDic = new Dictionary<string, DynamicImageTagetBehaviour>();
        private FilesManager pathManager;
        private GameObject subGameObject1;
		private GameObject subGameObject2;

        void Start()
        {
			Debug.Log ("ImageTargetManager started");
            if (!pathManager)
                pathManager = FindObjectOfType<FilesManager>();
            if (!subGameObject1)
            {
                subGameObject1 = Instantiate(Resources.Load("EasyAR", typeof(GameObject))) as GameObject;
                subGameObject1.SetActive(false);
			//	subGameObject2 = Instantiate(Resources.Load("TransparentVideo", typeof(GameObject))) as GameObject;
			//	subGameObject2.SetActive(false);
            }
        }

        void Update()
        {
            var imageTargetName_FileDic = pathManager.GetDirectoryName_FileDic();
            foreach (var obj in imageTargetName_FileDic.Where(obj => !imageTargetDic.ContainsKey(obj.Key)))
            {

                GameObject imageTarget = new GameObject(obj.Key);
                imageTarget.AddComponent<DynamicImageTagetBehaviour>().Init(StorageType.Absolute, obj.Value.Replace(@"\", "/"), obj.Key);
                imageTargetDic.Add(obj.Key, imageTarget.GetComponent<DynamicImageTagetBehaviour>());
                imageTarget.GetComponent<DynamicImageTagetBehaviour>().subGameObject = subGameObject1;
            }
        }

        public void ClearAllTarget()
        {
            subGameObject1.transform.parent = null;
            subGameObject1.SetActive(false);
            foreach (var obj in imageTargetDic)
                Destroy(obj.Value.gameObject);
            imageTargetDic = new Dictionary<string, DynamicImageTagetBehaviour>();
        }
    }
}
