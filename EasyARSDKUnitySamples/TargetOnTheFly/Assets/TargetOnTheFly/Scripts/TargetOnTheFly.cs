﻿/**
* Copyright (c) 2015 VisionStar Information Technology (Shanghai) Co., Ltd. All Rights Reserved.
* EasyAR is the registered trademark or trademark of VisionStar Information Technology (Shanghai) Co., Ltd in China
* and other countries for the augmented reality technology developed by VisionStar Information Technology (Shanghai) Co., Ltd.
*/

using UnityEngine;
using System.Collections;
using EasyAR;
using System.IO;

namespace EasyARSample
{
    public class TargetOnTheFly : MonoBehaviour
    {
        [TextArea(1, 10)]
		public string Key = "";
        private const string title = "Pleaser enter KEY first!";
		private const string boxtitle = "";
        private const string keyMessage = ""
            + "Steps to create the key for this sample:\n"
            + "  1. login www.easyar.com\n"
            + "  2. create app with\n"
            + "      Name: TargetOnTheFly (Unity)\n"
            + "      Bundle ID: cn.easyar.samples.unity.targetonthefly\n"
            + "  3. find the created item in the list and show key\n"
            + "  4. replace all text in TextArea with your key";

        [HideInInspector]
        public bool StartShowMessage = false;
        private bool isShowing = false;
        private ImageTargetManager imageManager;
        private FilesManager imageCreater;
	
		public int selGridInt = 0;

        public GUISkin skin;
		string sPicture;
		Texture2D tex1 = null;
		Texture2D tex2 = null;
		Texture[] seltext;
		void LoadButtons()
		{

			byte[] fileData1;
			byte[] fileData2;
			DownloadContent.IMAGE_FOLDER =  Application.persistentDataPath + "/Images";
			string path1 = DownloadContent.IMAGE_FOLDER + "/1.jpg";
			string path2 = DownloadContent.IMAGE_FOLDER + "/2.jpg";
		

				
				fileData1 = File.ReadAllBytes (path1);
				
				tex1 = new Texture2D (100, 100);
				tex1.LoadImage (fileData1); //..this will auto-resize the texture dimensions.

			fileData2 = File.ReadAllBytes (path2);
			
			tex2 = new Texture2D (100, 100);
			tex2.LoadImage (fileData2); //..this will auto-resize the texture dimensions.
			seltext = new Texture[2];
			seltext [0] = tex1;
			seltext [1] = tex2;


		}
        private void Awake()
        {
          /*  if (Key.Contains(boxtitle))
            {
#if UNITY_EDITOR
                UnityEditor.EditorUtility.DisplayDialog(title, keyMessage, "OK");
#endif
                Debug.LogError(title + " " + keyMessage);
            } */
			LoadButtons ();

            ARBuilder.Instance.InitializeEasyAR(Key);
            ARBuilder.Instance.EasyBuild();
            imageManager = FindObjectOfType<ImageTargetManager>();
            imageCreater = FindObjectOfType<FilesManager>();

        }

        void OnGUI()
        {

            if (StartShowMessage)
            {
                if (!isShowing)
                    StartCoroutine(showMessage());
                StartShowMessage = false;
            }

            GUI.Box(new Rect(Screen.width / 2 - 250, 30, 500, 60), "Select a Picture to augment", skin.GetStyle("Box"));
			//GUI.SelectionGrid(new Rect(Screen.width/2 - 150,30,500,60),selected:,
			selGridInt = GUI.SelectionGrid(new Rect (25, 25, 200, 200), selGridInt, seltext, 2);
			sPicture = (selGridInt + 1).ToString() + ".jpg";
			GUI.Label (new Rect (0, 0, 100, 100), sPicture);

            GUI.Box(new Rect(Screen.width / 4, Screen.height / 4, Screen.width / 2, Screen.height / 2), "", skin.GetStyle("Box"));

            if (isShowing)
                GUI.Box(new Rect(Screen.width / 2 - 65, Screen.height / 2, 130, 60), "Photo Saved", skin.GetStyle("Box"));


            if (GUI.Button (new Rect (Screen.width / 2 - 80, Screen.height - 85, 160, 80), "Take Photo", skin.GetStyle ("Button"))) {
				imageCreater.ClearTexture();
				imageCreater.StartTakePhoto (sPicture);
			}
            if (GUI.Button(new Rect(Screen.width - 160, Screen.height - 85, 150, 80), "Clear Targets", skin.GetStyle("Button")))
            {
                imageCreater.ClearTexture();
                imageManager.ClearAllTarget();
            }
        }

        IEnumerator showMessage()
        {
            isShowing = true;
            yield return new WaitForSeconds(2f);
            isShowing = false;
        }
    }
}
