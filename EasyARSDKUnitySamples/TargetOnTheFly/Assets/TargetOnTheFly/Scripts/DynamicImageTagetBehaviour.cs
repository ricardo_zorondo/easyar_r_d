﻿/**
* Copyright (c) 2015 VisionStar Information Technology (Shanghai) Co., Ltd. All Rights Reserved.
* EasyAR is the registered trademark or trademark of VisionStar Information Technology (Shanghai) Co., Ltd in China
* and other countries for the augmented reality technology developed by VisionStar Information Technology (Shanghai) Co., Ltd.
*/

using UnityEngine;
using EasyAR;
using System.IO;


namespace EasyARSample
{

    public class DynamicImageTagetBehaviour : ImageTargetBehaviour, ITargetEventHandler
    {
		private string video = @"http://7xl1ve.media1.z0.glb.clouddn.com/sdkvideo/EasyARSDKShow201520.mp4";

        public GameObject subGameObject = null;
		public GameObject videoObject = null;
		Renderer rend = null;
		public void LoadVideo()
		{
			if (videoObject == null) {
			
				videoObject = Instantiate (Resources.Load ("VideoPlayer", typeof(GameObject))) as GameObject;
				videoObject.transform.parent = this.transform;
				videoObject.transform.localPosition = new Vector3 (0, 0.225f, 0);
				videoObject.transform.localRotation = new Quaternion ();
				videoObject.transform.localScale = new Vector3 (0.8f, 0.45f, 0.45f);
			
				VideoPlayerBaseBehaviour videoPlayer = videoObject.GetComponent<VideoPlayerBaseBehaviour> ();
				if (videoPlayer) {
					videoPlayer.Storage = StorageType.Absolute;
					videoPlayer.Path = video;
					videoPlayer.EnableAutoPlay = true;
					videoPlayer.EnableLoop = true;
					videoPlayer.Open ();
				}
			} else {
				return;
			}
		}
		
		void HideObjects(Transform trans)
		{
			if (videoObject == null)
				return;
			for (int i = 0; i < trans.childCount; ++i)
				HideObjects(trans.GetChild(i));
			if (videoObject.transform != trans)
				videoObject.gameObject.SetActive(false);
		}
		
		void ShowObjects(Transform trans)
		{
			if (videoObject == null)
				return;
			for (int i = 0; i < trans.childCount; ++i)
				ShowObjects(trans.GetChild(i));
			if (videoObject.transform != trans)
				videoObject.gameObject.SetActive(true);
		}

        public void Init(StorageType type, string path, string name)
        {
		//	DownloadContent.TARGET_FOLDER = Application.persistentDataPath + "/Targets";
		//	DownloadContent.IMAGE_FOLDER = Application.persistentDataPath + "/Images";
		//	DownloadContent.SetupExternalFolders ();
			Path = path;
            Name = name;
            Storage = type;
		//	LoadVideo ();
            Bind(ARBuilder.Instance.TrackerBehaviours[0]);

        }
		public static Texture2D LoadPNG(string filePath,Transform transform) {
			
			Texture2D tex = null;
			byte[] fileData;
			string fullpath = DownloadContent.IMAGE_FOLDER + "/" + filePath + ".jpg";
			Debug.Log ("texture full path = " + fullpath);
			if (File.Exists (fullpath)) {
				Bounds bounds = RectTransformUtility.CalculateRelativeRectTransformBounds(transform);

				Debug.Log ("file exists= " + fullpath);
				fileData = File.ReadAllBytes (fullpath);

				tex = new Texture2D (20, 20);
				tex.LoadImage (fileData); //..this will auto-resize the texture dimensions.
			} else {
				Debug.Log ("file does not exists= " + fullpath);
			}
			return tex;
		}
        void ITargetEventHandler.OnTargetFound(Target imageTarget)
        {
			Application.OpenURL("www.appearition.com");
		/*	if (imageTarget.Name.Equals ("photo635893210094719210-0.3781158")) {
				Debug.Log ("target found");

				Application.OpenURL("www.appearition.com");
			} else { */
			Debug.Log ("target found");
				if(subGameObject == null)
					subGameObject = Instantiate (Resources.Load ("EasyAR", typeof(GameObject))) as GameObject;
				if(rend == null)
					rend = subGameObject.GetComponent<Renderer>(); 

				rend.material.mainTexture = LoadPNG (imageTarget.Name,rend.transform);
				
				Debug.Log ("imageTarget found = " + imageTarget.Name);
				if(videoObject != null)
					ShowObjects(videoObject.transform);


				subGameObject.transform.parent = rend.transform;
				subGameObject.SetActive(true);
				Debug.Log ("ImageTarget = " + imageTarget.Name.ToString ()); 
			//} 

        }
		public void ReloadTracker(){
			if (subGameObject != null) {
				if (subGameObject) {
					subGameObject.SetActive (false);
				
				}
			}
			if(videoObject != null)
				HideObjects(videoObject.transform);
		}
        void ITargetEventHandler.OnTargetLost(Target imageTarget)
        {
            if (subGameObject) {
				subGameObject.SetActive (false);

			}
			if(videoObject != null)
				HideObjects(videoObject.transform);
        }

        void ITargetEventHandler.OnTargetLoad(Target target, bool status)
        {
        }

        void ITargetEventHandler.OnTargetUnload(Target target, bool status)
        {
        }
    }
}
